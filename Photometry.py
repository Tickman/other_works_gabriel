# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 18:20:01 2018

@author: bella
"""
from pylab import *
from tkinter import *
from tkinter import ttk
from tkinter.filedialog import *
from tkinter import messagebox
from astropy.io import fits as f
from PIL import Image,ImageTk
import cv2
import csv

#This constant is he one used to calculate magntiudes. The default value will be 0.
global C
C = 0

# =============================================================================
# IMPORTANT FUNCTIONS
# =============================================================================

#Correction of a raw image using darks and flats
def correction(darklist,flatlist,raw,win):
    #I create some arrays that contain the images bounded to keys from the dictionary
    arraydark = [dictnb[x] for x in darklist]
    arrayflat = [dictnb[x] for x in flatlist]
    arrayraw = [dictnb[x] for x in raw]
    #Create the masters
    masterdark = (median(arraydark,axis=0))
    masterflat = (median(arrayflat,axis=0))
    #A method for normalizing is to divide the masterflat and the median before normalizing the raw image
    medianflat = median(median(masterflat-masterdark,axis=1),axis=0)
    flatnorm = (masterflat-masterdark)/medianflat
    rawdark = arrayraw[0]-masterdark
    rawdark[rawdark<0] = 0
    #Raw fully corrected
    rawcorr = (rawdark/flatnorm)
    #To not worry, I eliminate negative numbers and numbers above the maximum (2**16-1)
    rawcorr[rawcorr<0] = 0
    rawcorr[rawcorr>65535] = 65535
    rawcorr = rawcorr.astype('uint16')
    #Save as a .fit file
    file = f.PrimaryHDU(rawcorr)
    filel = f.HDUList([file])
    filesave = asksaveasfilename(defaultextension='.fit',filetypes = (('FITS files','*.fit'),('All files','*.*')))
    filel.writeto('%s' % (filesave),overwrite=True)
    popup_done(root)
    win.destroy()
    
#Function that applies alignment to a list of images.
def alignment(alignlist,alwin):
    alwin.iconify()
    keylist = [*dictnb]
    tabidlist = nbook.tabs()
    poslist = []
    posvar = DoubleVar(value=poslist)
    #This try/except is used to avoid copies of the info window.
    try:
        if inwin.state() == 'normal':
           inwin.destroy()
    except:
        pass
    infowin()
    def mousepos(event):
        #Obtains position on canvas aka image
        xx = int(canvas.canvasx(event.x))
        yy = int(canvas.canvasy(event.y))
        #The variable is not needed, so we put a random number to let tkinter
        #continue doing things (due to wait_variable)
        posvar.set(1)
        poslist.append([xx,yy])
    #This for is used because we need three points in every image to align
    for k in range(0,3):
        #This for is used to get the pixel of the images to align
        for j in range(0,len(alignlist)):
            #This for is used to select the images for alignment
            for i in range(0,len(tabidlist)):
                if alignlist[j] == nbook.tab(tabidlist[i],option='text'):
                    nbook.select(tabidlist[i]) 
                    break
            canvas = nbook.winfo_children()[i].winfo_children()[2]
            canvas.bind('<Button-1>',lambda event: mousepos(event))
            #wait_variable allows to do something while tkinter waits for a change in a variable
            canvas.wait_variable(posvar)
            

    #'For' to unbind at the end all canvas for future purposes        
    for ii in range(0,len(tabidlist)):
        nbook.winfo_children()[ii].winfo_children()[2].unbind('<Button-1>')
    #image0 is the reference image in which the align will be done taking the first one
    image0 = dictnb[alignlist[0]]
    # List containing the selected points in the first image
    point0 = array([poslist[x] for x in range(0,len(poslist),len(alignlist))], dtype = 'f')
    for jj in range(1,len(alignlist)):
        points = array([poslist[x] for x in range(jj,len(poslist),len(alignlist))], dtype = 'f')
        #Matrix of the affine transformation. It is a combination of translation and rotation
        M = cv2.getAffineTransform(points,point0)
        #We apply the matrix to the image to align
        aligned = cv2.warpAffine(dictnb[alignlist[jj]],M,dsize=(image0.shape[0],image0.shape[1]))
        # The it is save as .fit
        file = f.PrimaryHDU(aligned)
        filel = f.HDUList([file])
        filesave = asksaveasfilename(defaultextension='.fit',filetypes = (('FITS files','*.fit'),('All files','*.*')))
        filel.writeto('%s' % (filesave),overwrite=True)
    popup_done(root)
    canvas.unbind('<Button-1>')
    alwin.destroy()

def calibration(main,second,win):
    win.iconify()
    tabidlist = nbook.tabs()
    global incircle
    global outcircle
    global canvas
    global radius
    global C
    global inwin
    incircle = 0
    outcircle = 0
    #List with all the calculated constants for every star(and for every image.
    #In the end, and average will be done
    list_constant=[]
    # radius will be an IntVar so we can change it and use it anytime
    radius = IntVar()
    radius.set(12)
    mainim = dictnb[main]
    secim = [dictnb[x] for x in second]
    
    def motion(event):
        #Info part
        x0 = int(canvas.canvasx(event.x))
        y0 = int(canvas.canvasy(event.y))
        poslabel['text'] = 'Position (x,y): %d, %d' % (x0,y0)
        countlabel['text'] = 'Counts: %d' % (mainim[y0,x0])
        #Red circle part
        x, y = event.x+2, event.y+2
        global incircle
        global outcircle
        global radius
        #The circle is deleted and created everytime the cursor moves
        canvas.delete(incircle)
        canvas.delete(outcircle)
        rad = radius.get()
        x_max = x + rad
        x_min = x - rad
        y_max = y + rad
        y_min = y - rad
        #This is the position of the scrollbar that must be added to the coordinates
        #of the circle to keep it always with the mouse
        xsc = xscroll.get()[0]
        ysc = yscroll.get()[0]
        #The calibration nees two circles: the inner one and the outer one.
        incircle = canvas.create_oval(x_max+xsc*4096, y_max+ysc*4096, x_min+xsc*4096, y_min+ysc*4096, outline="red")
        outcircle = canvas.create_oval(x_max+1.5*rad+xsc*4096,y_max+1.5*rad+ysc*4096,x_min-1.5*rad+xsc*4096,y_min-1.5*rad+ysc*4096, outline='red')
    #This loop is used to select automatically the main file
    for i in range(0,len(tabidlist)):
                if main == nbook.tab(tabidlist[i],option='text'):
                    nbook.select(tabidlist[i])
                    break
    canvas = nbook.winfo_children()[i].winfo_children()[2]
    xscroll = nbook.winfo_children()[i].winfo_children()[0]
    yscroll = nbook.winfo_children()[i].winfo_children()[1]
    #This try/except is used when the info window (not alt) is opened. If inwin
    #does not exists yet, an error occurs, so we may need an 'except'.
    try:
        if inwin.state() == 'normal':
           inwin.destroy()
    except:
        pass
    #Info window (just inside this function)
    inwinalt = Toplevel()
    inwinalt.title('Info')
    inwinalt.geometry('200x200+1000+400')
    inwinalt.resizable(False,False)
    inwinalt.attributes('-topmost', True)
    #Bind that destroys the window when tab changes
    poslabel = Label(inwinalt)
    poslabel.grid(column=0,row=1,sticky=W)
    countlabel = Label(inwinalt)
    countlabel.grid(column=0,row=2,sticky=W)
    maxlabel = Label(inwinalt)
    maxlabel.grid(column=0,row=3,sticky=W)
    minlabel = Label(inwinalt)
    minlabel.grid(column=0,row=4,sticky=W)
    namelabel = Label(inwinalt)
    namelabel.grid(column=0,row=0,sticky=W)
    maxlabel['text'] = 'Maximum: %d' % (amax(mainim))
    minlabel['text'] = 'Minimum: %d' % (amin(mainim))
    namelabel['text'] = 'Name: %s' % (main)
    redcircle = canvas.bind('<Motion>',motion)
        
    #Pop-up menu to change radius
    popupmenu = Menu(canvas,tearoff=0)
    menu_radius = Menu(popupmenu)
    popupmenu.add_cascade(menu=menu_radius,label='Radius')
    #This function changes de radius using an IntVar
    def change_radius(t):
        radius.set(t)
    #Commands must be added for all possible radius because inside a loop won't work well
    menu_radius.add_command(label='10',command = lambda: change_radius(10))
    menu_radius.add_command(label='12',command = lambda: change_radius(12))
    menu_radius.add_command(label='14',command = lambda: change_radius(14))
    menu_radius.add_command(label='16',command = lambda: change_radius(16))
    menu_radius.add_command(label='18',command = lambda: change_radius(18))
    menu_radius.add_command(label='20',command = lambda: change_radius(20))
    menu_radius.add_command(label='22',command = lambda: change_radius(22))
    #This function calls the radius menu when someone right-click during calibration
    def show_popup(event):
        try:
            popupmenu.tk_popup(event.x+38,event.y+78,0)
        finally:
            popupmenu.grab_release()
    but3 = canvas.bind('<Button-3>',show_popup)
        
    #Function that opens a window with an entry to introduce the magnitude of
    #the star to calculate the constant
    def get_counts(event):
        #This function gets the total counts inside the inner circle given
        #the sky noise of the outer circle. It  will run when a magnitude is introduced
        def set_mag():
            magnitude.set(magentry.get())
            popentry.destroy()
            #We create two circular masks around the coordinates to obtain the 
            #number of counts of the star
            x, y = meshgrid(linspace(0,1,4096),linspace(0,1,4096))
            rad = radius.get()
            R = sqrt((x-(x0/4096))**2+(y-(y0/4096))**2)
            #Masks to get the total counts inside the circles
            R0in = R<(rad/4096)
            R0out = R<(rad*1.5/4096)
            #Counts inside the inner circle
            countin = sum(sum(R0in*mainim))
            #Counts inside the outer circle
            countout = sum(sum(R0out*mainim))
            totalcounts = countin-(countout-countin)/((1.5)**2-1)
            #This is how the constant is calculated
            const = magnitude.get()+2.5*log10(totalcounts)
            #If there is no secondary images then use only the main one
            if not second:
                list_constant.append(const)
            else:
                countinsec = sum(sum(R0in*secim,axis=1),axis=1)
                countoutsec = sum(sum(R0out*secim,axis=1),axis=1)
                countin_pixelsec = countinsec/(sum(sum(R0in)))
                countout_pixelsec = countoutsec/(sum(sum(R0out)))
                totalcountssec = (countin_pixelsec-countout_pixelsec)*sum(sum(R0in))
                for i in totalcountssec:
                    const += magnitude.get()+2.5*log10(i)     
                #The average is the sum of the constants get in every image divided
                #by the length of the list of secondary constants plus 1(the main image)
                const /= (len(totalcountssec)+1)
                list_constant.append(const)
                
        #Window where the magnitude will be introduced
        popentry = Toplevel()
        popentry.resizable(False,False)
        popentry.attributes('-topmost', True)
        popentry.update()
        popentry.attributes('-topmost', False)
        popentry.grab_set()
        #To center the window
        w = root.winfo_reqwidth()
        h = root.winfo_reqheight()
        ws = root.winfo_screenwidth()
        hs = root.winfo_screenheight()
        x = (ws/2) - (w/2)
        y = (hs/2) - (h/2)
        popentry.geometry('150x60+%d+%d' % (x, y))
        magframe = ttk.Frame(popentry)
        magframe.grid(sticky=NSEW)
        #Coordinates where the pointer is
        x0 = int(canvas.canvasx(event.x))
        y0 = int(canvas.canvasy(event.y))
        magnitude = DoubleVar()
        magentry = ttk.Entry(magframe,textvariable=magnitude)
        magentry.grid()
        magentry.grid_columnconfigure(0,weight=1)
        magentry.grid_rowconfigure(0,weight=1)
        magentry.focus()
        #After pressing Return(or 'Entry') the calculation begins
        magentry.bind('<Return>',lambda e: set_mag())
    
    but1 = canvas.bind('<Button-1>',get_counts)
    #Function that ends the calibration
    def end_cal(event):
        global C
        #Unbind every bind on canvas for future purposes
        canvas.unbind('<Button-1>',but1)
        canvas.unbind('<Motion>',redcircle)
        canvas.unbind('<Button-3>',but3)
        canvas.delete(incircle)
        canvas.delete(outcircle)
        win.destroy()
        inwinalt.destroy()
        poslabel.destroy()
        countlabel.destroy()
        maxlabel.destroy()
        minlabel.destroy()
        namelabel.destroy()
        #Final Constant
        C = sum(list_constant)/len(list_constant)
        popup_done(root)
    root.bind('<Escape>', end_cal)

#Function that computates the magnitude from selected stars. It is similar to
#calibration function
def get_magnitude(main,second,win):
    win.iconify()
    tabidlist = nbook.tabs()
    global incircle
    global outcircle
    global canvas
    global radius
    global number
    global C
    incircle = 0
    outcircle = 0
    # radius will be an IntVar so we can change it and use it anytime
    radius = IntVar()
    radius.set(12)
    mainim = dictnb[main]
    secim = [dictnb[x] for x in second]
    
    def motion(event):
        #Info part
        x0 = int(canvas.canvasx(event.x))
        y0 = int(canvas.canvasy(event.y))
        poslabel['text'] = 'Position (x,y): %d, %d' % (x0,y0)
        countlabel['text'] = 'Counts: %d' % (mainim[y0,x0])
        #Red circle part
        x, y = event.x+2, event.y+2
        global incircle
        global outcircle
        global radius
        #The circle is deleted and created everytime the cursor moves
        canvas.delete(incircle)
        canvas.delete(outcircle)
        rad = radius.get()
        x_max = x + rad
        x_min = x - rad
        y_max = y + rad
        y_min = y - rad
        #This is the position of the scrollbar that must be added to the coordinates
        #of the circle to keep it always with the mouse
        xsc = xscroll.get()[0]
        ysc = yscroll.get()[0]
        #The calibration nees two circles: the inner one and the outer one.
        incircle = canvas.create_oval(x_max+xsc*4096, y_max+ysc*4096, x_min+xsc*4096, y_min+ysc*4096, outline="red")
        outcircle = canvas.create_oval(x_max+1.5*rad+xsc*4096,y_max+1.5*rad+ysc*4096,x_min-1.5*rad+xsc*4096,y_min-1.5*rad+ysc*4096, outline='red')
    #This loop is used to select automatically the main file
    for i in range(0,len(tabidlist)):
                if main == nbook.tab(tabidlist[i],option='text'):
                    nbook.select(tabidlist[i])
                    break
    canvas = nbook.winfo_children()[i].winfo_children()[2]
    xscroll = nbook.winfo_children()[i].winfo_children()[0]
    yscroll = nbook.winfo_children()[i].winfo_children()[1]
    #This try/except is used when the info window (not alt) is opened. If inwin
    #does not exists yet, an error occurs, so we may need an 'except'.
    try:
        if inwin.state() == 'normal':
           inwin.destroy()
    except:
        pass
    #Info window (just inside this function)
    inwinalt = Toplevel()
    inwinalt.title('Info')
    inwinalt.geometry('200x200+1000+400')
    inwinalt.resizable(False,False)
    inwinalt.attributes('-topmost', True)
    #Bind that destroys the window when tab changes
    poslabel = Label(inwinalt)
    poslabel.grid(column=0,row=1,sticky=W)
    countlabel = Label(inwinalt)
    countlabel.grid(column=0,row=2,sticky=W)
    maxlabel = Label(inwinalt)
    maxlabel.grid(column=0,row=3,sticky=W)
    minlabel = Label(inwinalt)
    minlabel.grid(column=0,row=4,sticky=W)
    namelabel = Label(inwinalt)
    namelabel.grid(column=0,row=0,sticky=W)
    maxlabel['text'] = 'Maximum: %d' % (amax(mainim))
    minlabel['text'] = 'Minimum: %d' % (amin(mainim))
    namelabel['text'] = 'Name: %s' % (main)
    redcircle = canvas.bind('<Motion>',motion)
    
    #Pop-up menu to change radius
    popupmenu = Menu(canvas,tearoff=0)
    menu_radius = Menu(popupmenu)
    popupmenu.add_cascade(menu=menu_radius,label='Radius')
    #This function changes de radius using an IntVar
    def change_radius(t):
        radius.set(t)
    #Commands must be added for all possible radius because inside a loop won't work well
    menu_radius.add_command(label='10',command = lambda: change_radius(10))
    menu_radius.add_command(label='12',command = lambda: change_radius(12))
    menu_radius.add_command(label='14',command = lambda: change_radius(14))
    menu_radius.add_command(label='16',command = lambda: change_radius(16))
    menu_radius.add_command(label='18',command = lambda: change_radius(18))
    menu_radius.add_command(label='20',command = lambda: change_radius(20))
    menu_radius.add_command(label='22',command = lambda: change_radius(22))
    #This function calls the radius menu when someone right-click during calibration
    def show_popup(event):
        try:
            popupmenu.tk_popup(event.x+38,event.y+78,0)
        finally:
            popupmenu.grab_release()
    but3 = canvas.bind('<Button-3>',show_popup)

    number = 1
    csvfile = open('magnitude.csv','w')
    magwriter = csv.writer(csvfile,delimiter=';')
    magwriter.writerow(['Number','Pixel (x)','Pixel (y)','Magnitude','Total counts'])
    
    #Function to calculate the magnitude of the selected star and writes it on a
    #csv file followed by other info. The process is very similar to the function
    #to calculate the constant.
    def mag_in_csv(event):
        global number
        x0 = int(canvas.canvasx(event.x))
        y0 = int(canvas.canvasy(event.y))
        x, y = meshgrid(linspace(0,1,4096),linspace(0,1,4096))
        rad = radius.get()
        R = sqrt((x-(x0/4096))**2+(y-(y0/4096))**2)
        R0in = R<(rad/4096)
        R0out = R<(rad*1.5/4096)
        countin = sum(sum(R0in*mainim))
        countout = sum(sum(R0out*mainim))
        totalcounts = countin-(countout-countin)/((1.5)**2-1)
        #This try/except is used when totalcounts is 0 and avoid problems with the logarithm
        try:
            magnitude = C-2.5*log10(totalcounts)
        except:
            magnitude = C
        if not second:
            magwriter.writerow(['%d' % (number),'%d' % (x0),'%d' % (y0),'%.3f' % (magnitude),'%d' % (totalcounts)])
        else:
            countinsec = sum(sum(R0in*secim,axis=1),axis=1)
            countoutsec = sum(sum(R0out*secim,axis=1),axis=1)
            countin_pixelsec = countinsec/(sum(sum(R0in)))
            countout_pixelsec = countoutsec/(sum(sum(R0out)))
            totalcountssec = (countin_pixelsec-countout_pixelsec)*sum(sum(R0in))
            for i in totalcountssec:
                magnitude += (C-2.5*log10(i))
            magnitude /= (len(totalcountssec)+1)
            totalcountsaverage = totalcounts
            for i in totalcountssec:
                totalcountsaverage += i
            magwriter.writerow(['%d' % (number),'%d' % (x0),'%d' % (y0),'%.3f' % (magnitude),'%d' % (totalcountsaverage)])
        number += 1
    canvas.bind('<Button-1>',mag_in_csv)
    
    #Function that ends the process of getting magnitudes and closes the csv file
    def end_mag(event):
        canvas.unbind('<Button-1>')
        canvas.unbind('<Motion>')
        canvas.unbind('<Button-3>')
        canvas.delete(incircle)
        canvas.delete(outcircle)
        win.destroy()
        inwinalt.destroy()
        poslabel.destroy()
        countlabel.destroy()
        maxlabel.destroy()
        minlabel.destroy()
        namelabel.destroy()
        csvfile.close()
        popup_done(root)
    root.bind('<Escape>', end_mag)
    
    
# =============================================================================
# AUXILIARY FUNCTIONS
# =============================================================================

# Function for buttons to pass a file from one list to another
def to_list(listfiles,lista):
    if len(listfiles.curselection()) != 0:
        x = listfiles.get(listfiles.curselection())
        lista.insert(END, x)
    else:
        popup_error(root)

#Function for Pop-up window when no file was selected
def popup_error(rootcor):
    pwin = Toplevel()
    pwin.title=('Error')
    pwin.resizable(False,False)
    pwin.overrideredirect(1)
    pwin.grab_set()
    pframe = Frame(pwin,highlightthickness=1,highlightbackground='black',relief='raised')
    pframe.grid(sticky=NSEW)
    x = rootcor.winfo_x()
    y = rootcor.winfo_y()
    w = rootcor.winfo_height()
    h = rootcor.winfo_width()
    pwin.geometry("%dx%d+%d+%d" % (127, 49, x+h/2-60, y+w/2-25))
    lab = Label(pframe,text='Select an element first.').grid(column=0,row=0,sticky=NSEW)
    but = Button(pframe,text='Okay',command = lambda: pwin.destroy())
    but.grid(column=0,row=1)

#Function when something is done
def popup_done(root):
    win = Toplevel()
    win.title=('Correction')
    win.resizable(False,False)
    win.overrideredirect(1)
    win.grab_set()
    x = root.winfo_x()
    y = root.winfo_y()
    w = root.winfo_height()
    h = root.winfo_width()
    win.geometry("%dx%d+%d+%d" % (100,50,x+h/2-50,y+w/2-25))
    but = Button(win,text='Done',command = lambda: win.destroy())
    but.pack(fill=None, expand=True)

#Notebook
def noteopen(nbook):
    #Open .fit and put it in the dictionary
    filename = askopenfilename(defaultextension='.fit',filetypes=[('FITS file','*.fit'), ('All files','*.*')])
    file = (f.open(filename))[0].data
    dictnb[os.path.basename(filename)]=file
    #Downgrade the image that will be shown due to limitations of .png files.
    imfile = (file/2**8).astype('uint8')
    im = Image.fromarray(imfile,mode = 'L')
    img = ImageTk.PhotoImage(image = im)
    #Frame with scrollbars for image.
    fr =ttk.Frame(nbook)
    fr.pack(expand=True,fill=BOTH)
    xscrollbar = ttk.Scrollbar(fr,orient=HORIZONTAL)
    xscrollbar.pack(side=BOTTOM,fill=X)
    yscrollbar = ttk.Scrollbar(fr,orient=VERTICAL)
    yscrollbar.pack(side=RIGHT,fill=Y)
    #Canvas where the image is.
    canv = Canvas(fr)
    canv.pack(side=LEFT, expand=True, fill=BOTH)
    canv.config(highlightthickness=0)
    canv.config(width=400, height=200)
    yscrollbar.config(command=canv.yview)
    xscrollbar.config(command=canv.xview)
    canv.config(yscrollcommand=yscrollbar.set)
    canv.config(xscrollcommand=xscrollbar.set)
    canv.config(scrollregion=(0,0,4096,4096))
    canv.image = img
    canv.create_image(0,0,image=img,anchor=NW)
    nbook.add(fr,text=os.path.basename(filename))

#Function to close tabs on the notebook   by right-clicking it. 
def click_close(event):
    clicked = nbook.tk.call(nbook._w, "identify", "tab", event.x, event.y)
    active = nbook.index(nbook.select())
    tabid = nbook.select()
    name = nbook.tab(tabid,option='text')
    if clicked == active:
        del dictnb[name]
        nbook.forget(clicked)

# =============================================================================
# WINDOW FUNCTIONS
# =============================================================================

#Calibration window
def correctionwin():
    #Window for correction
    rootcor = Toplevel()
    rootcor.title('Correction')
    rootcor.geometry('450x380+600+200')
    rootcor.resizable(False,False)
    corframe = ttk.Frame(rootcor)
    corframe.grid(sticky=NSEW)
    #Labels to listboxes
    ttk.Label(corframe,text = 'Open files', font = 'Arial 12').grid(column = 0,row=0,pady=10)
    ttk.Label(corframe, text = 'Dark', font = 'Arial 12').grid(column = 2,row = 0,columnspan=1)
    ttk.Label(corframe,text = 'Flat', font = 'Arial 12').grid(column = 2,row = 2,columnspan=1)
    ttk.Label(corframe,text = 'Raw', font = 'Arial 14').grid(column = 1,row = 5,columnspan=1)
    darks = []
    flats = []
    raw = []
    listd = Variable(value=darks)
    listf = Variable(value=flats)
    listr = Variable(value=raw)
    #List for files, etc.
    listfiles = Listbox(corframe,height=12,selectmode=SINGLE)
    listfiles.insert(END,*sorted(dictnb))
    listfiles.grid(column=0,row=1,rowspan=3,padx=20)
    listdark = Listbox(corframe,listvariable=listd,height=5,selectmode=SINGLE)
    listdark.grid(column=2,row=1,padx=20)
    listflat = Listbox(corframe,listvariable=listf,height=5,selectmode=SINGLE)
    listflat.grid(column=2,row=3,padx=20)
    listraw = Listbox(corframe,listvariable=listr,height=1,selectmode=SINGLE)
    listraw.grid(column=1,row=6,columnspan=1)
    #Buttons
    butdark = ttk.Button(corframe,text='To Dark ->',command = lambda: to_list(listfiles,listdark))
    butdark.grid(column=1,row=1)
    butflat = ttk.Button(corframe,text='To Flat ->',command = lambda: to_list(listfiles,listflat))
    butflat.grid(column=1,row=3)
    butraw = ttk.Button(corframe,text='To Raw\n      |\n     V',command = lambda: to_list(listfiles,listraw))
    butraw.grid(column=1,row=4)
    #This button calls the function correction. Variable.get gives a tuple, so it must be converted into a list
    butcorr = ttk.Button(corframe,text='Correct!',style='My.TButton',width=10,command= lambda: correction(list(listdark.get(0,END)),list(listflat.get(0,END)),list(listraw.get(0,END)),rootcor))
    butcorr.grid(column=2,row=6)

#Alignment Window 
def alignwin():
    alwin = Toplevel()
    alwin.title('Align')
    alwin.geometry('400x300+500+150')
    alwin.resizable(False,False)
    frame = ttk.Frame(alwin)
    frame.grid(sticky=(NSEW))
    align = []
    lalign = Variable(value=align)
    filelabel = ttk.Label(frame,text= 'Open files', font = 'Arial 14').grid(column=0,row=0,padx=10,pady=10)
    alignlabel = ttk.Label(frame,text='Alignment files', font = 'Arial 14').grid(column=2,row=1)
    listfiles = Listbox(frame,height=12, selectmode=SINGLE)
    listfiles.insert(END,*sorted(dictnb))
    listfiles.grid(column=0,row=1,rowspan = 3,padx=10,pady=10)
    listalign = Listbox(frame,height=5, selectmode=SINGLE)
    listalign.grid(column=2,row=2,padx=10,pady=10)
    toalignbut = ttk.Button(frame,text='To Align ->',command = lambda: to_list(listfiles,listalign))
    toalignbut.grid(column=1,row=2)
    albutton = ttk.Button(frame,text='Align!',style='My.TButton',width = 8,command = lambda: alignment(list(listalign.get(0,END)),alwin))
    albutton.grid(column=1,row=4)

#Calibration Window   
def calibrationwin():
    calwin = Toplevel()
    calwin.title('Calibration')
    calwin.geometry('400x300+550+175')
    calwin.resizable(False,False)
    frame = ttk.Frame(calwin)
    frame.grid(sticky=NSEW)
    maincal = Variable()
    secondary = []
    #Listbox that contains open files, main file and secondary files
    lsec = Variable(value=secondary)
    filelabel = ttk.Label(frame,text = 'Open files',font = 'Arial 14').grid(column=0,row=0,padx=30,pady=10)
    mainlabel = ttk.Label(frame,text = 'Main file',font = 'Arial 14').grid(column=4,row=0)
    seclabel = ttk.Label(frame,text = 'Secondary files',font= 'Arial 14').grid(column=4,row=2)
    listfiles = Listbox(frame,height=12,selectmode=SINGLE)
    listfiles.insert(END,*sorted(dictnb))
    listfiles.grid(column=0,row=1,columnspan=3,rowspan=3,padx=10,pady=10)
    listmain = Listbox(frame,height=1,selectmode=SINGLE)
    listmain.grid(column=4,row=1)
    listsec = Listbox(frame,height=4,selectmode=SINGLE)
    listsec.grid(column=4,row=3)
    #Function to get filter
    def filterselection(event):
        value = combofilter.get()
    #Combobox to select filter
    combofilter = ttk.Combobox(frame,state='readonly')
    combofilter.grid(column=0,row=4,columnspan=3)
    combofilter['values'] = ['B','V']
    combofilter.bind('<<ComboboxSelected>>',filterselection)
    mainbut = ttk.Button(frame,text='To Main ->',command = lambda: to_list(listfiles,listmain)).grid(column=3,row=1)
    secbut = ttk.Button(frame,text='To Secondary \n           ->',command = lambda: to_list(listfiles,listsec)).grid(column=3,row=3)
    #Function to check if main image was selected
    def cal():
        if not list(listmain.get(0,END)):
            messagebox.showwarning('Main required','Select a main file to calibrate')
        else:
            calibration(listmain.get(0),list(listsec.get(0,END)),calwin)
            
    calbut = ttk.Button(frame,text='Calibrate!',style='My.TButton',width=9,command = lambda: cal())
    calbut.grid(column=4,row=4)
 
def getmagwin():
    magwin = Toplevel()
    magwin.title('Get Magnitude')
    magwin.geometry('450x300+550+175')
    magwin.resizable(False,False)
    frame = ttk.Frame(magwin)
    frame.grid(sticky=NSEW)
    maincal = Variable()
    secondary = []
    #Listbox that contains open files, main file and secondary files
    lsec = Variable(value=secondary)
    filelabel = ttk.Label(frame,text = 'Open files',font = 'Arial 14').grid(column=0,row=0,padx=30,pady=10)
    mainlabel = ttk.Label(frame,text = 'Main file',font = 'Arial 14').grid(column=4,row=0)
    seclabel = ttk.Label(frame,text = 'Secondary files',font= 'Arial 14').grid(column=4,row=2)
    listfiles = Listbox(frame,height=12,selectmode=SINGLE)
    listfiles.insert(END,*sorted(dictnb))
    listfiles.grid(column=0,row=1,columnspan=3,rowspan=3,padx=10,pady=10)
    listmain = Listbox(frame,height=1,selectmode=SINGLE)
    listmain.grid(column=4,row=1)
    listsec = Listbox(frame,height=4,selectmode=SINGLE)
    listsec.grid(column=4,row=3)
    mainbut = ttk.Button(frame,text='To Main ->',command = lambda: to_list(listfiles,listmain)).grid(column=3,row=1)
    secbut = ttk.Button(frame,text='To Secondary \n           ->',command = lambda: to_list(listfiles,listsec)).grid(column=3,row=3)
    #Function to check if main image was selected
    def magnit():
        if not list(listmain.get(0,END)):
            messagebox.showwarning('Main required','Select a main file to calibrate')
        else:
           get_magnitude(listmain.get(0),list(listsec.get(0,END)),magwin)
            
    calbut = ttk.Button(frame,text='Get Magnitude!',style='My.TButton',width=13,command = lambda: magnit())
    calbut.grid(column=3,row=5)
#Function that shows information about the image
def infowin():
    global inwin
    #Function to obtain info everytime on canvas
    def motioninfo(event):
        x0 = int(canvas.canvasx(event.x))
        y0 = int(canvas.canvasy(event.y))
        poslabel['text'] = 'Position (x,y): %d, %d' % (x0,y0)
        countlabel['text'] = 'Counts: %d' % (image[y0,x0])
        
    #Function that closes the info window and resets the open image.       
    def destroy():
        inwin.destroy()
        poslabel.destroy()
        countlabel.destroy()
        maxlabel.destroy()
        minlabel.destroy()
        namelabel.destroy()
        canvas.unbind('<Motion>',infobind)
        image = 0
    def destroyandmake():
        inwin.destroy()
        poslabel.destroy()
        countlabel.destroy()
        maxlabel.destroy()
        minlabel.destroy()
        namelabel.destroy()
        canvas.unbind('<Motion>',infobind)
        image = 0
        infowin()
    inwin = Toplevel()
    inwin.title('Info')
    inwin.geometry('200x200+1000+400')
    inwin.resizable(False,False)
    inwin.attributes('-topmost', True)
    inwin.protocol('WM_DELETE_WINDOW', lambda: destroy())
    #Bind that destroys the window when tab changes
    nbook.bind('<<NotebookTabChanged>>', lambda e: destroyandmake())
    poslabel = Label(inwin)
    poslabel.grid(column=0,row=1,sticky=W)
    countlabel = Label(inwin)
    countlabel.grid(column=0,row=2,sticky=W)
    maxlabel = Label(inwin)
    maxlabel.grid(column=0,row=3,sticky=W)
    minlabel = Label(inwin)
    minlabel.grid(column=0,row=4,sticky=W)
    namelabel = Label(inwin)
    namelabel.grid(column=0,row=0,sticky=W)
    #This 'try/except' is used when no image is shown(same as canvas not opened)
    try:
        index = nbook.index(nbook.select())
        image = dictnb[nbook.tab(nbook.select(),option='text')]
        name = nbook.tab(nbook.select(),option='text')
        canvas = nbook.winfo_children()[index].winfo_children()[2]
        maxlabel['text'] = 'Maximum: %d' % (amax(image))
        minlabel['text'] = 'Minimum: %d' % (amin(image))
        namelabel['text'] = 'Name: %s' % (name)
        infobind = canvas.bind('<Motion>', lambda e: motioninfo(e))
    except:
        messagebox.showwarning('Error','Open an image to see info.')
        inwin.destroy()
        
#Main Window   
root = Tk()
root.title('The True MaxIm DL')
root.option_add('*tearOff', FALSE)
root.state('zoomed')

#Notebook
nbook = ttk.Notebook()
nbook.pack(expand=True,fill=BOTH)
nbook.bind('<Button-3>',click_close)
dictnb =  {}
#Style of some buttons
s = ttk.Style()
s.configure('My.TButton',font = ('Arial','14','bold'))
#Menu
menubar = Menu(root)
menu_file = Menu(menubar)
menu_edit = Menu(menubar)
menu_photo = Menu(menubar)
menubar.add_cascade(menu=menu_file, label='File')
menubar.add_cascade(menu=menu_photo, label='Photometry')

menu_file.add_command(label = 'Open', command = lambda: noteopen(nbook))
menu_file.add_command(label = 'Exit', command = lambda: root.destroy())
menu_photo.add_command(label = 'Correction', command = correctionwin)
menu_photo.add_command(label = 'Align', command = alignwin)
menu_photo.add_command(label = 'Calibration', command = calibrationwin)
menu_photo.add_command(label='Get Magnitude',command = getmagwin)
menu_photo.add_command(label = 'Info', command = infowin)
root.config(menu=menubar)

root.mainloop()
