#include <iostream>
#include <array>
#include <random>
#include <ctime>

namespace Rand
{
	std::mt19937 mersenne(static_cast<std::mt19937::result_type>(std::time(nullptr))); //Seed
}

enum Rank
{
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE,
	MAXRANKS
};

enum Suits
{
	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES,
	MAXSUITS
};

struct Card
{
	Rank rank;
	Suits suits;
};

void printCard(const Card &card)
{
	switch (card.rank)
	{
		case TWO:
		case THREE:
		case FOUR:
		case FIVE:
		case SIX:
		case SEVEN:
		case EIGHT:
		case NINE:
		case TEN:
			std::cout << card.rank+2;
			break;
		case JACK:
			std::cout << "J";
			break;
		case QUEEN:
			std::cout << "Q";
			break;
		case KING:
			std::cout << "K";
			break;
		case ACE:
			std::cout << "A";
			break;
	}
	switch (card.suits)
	{
	case CLUBS:
		std::cout << "C";
		break;
	case DIAMONDS:
		std::cout << "D";
		break;
	case HEARTS:
		std::cout << "H";
		break;
	case SPADES:
		std::cout << "S";
		break;
	}
}

void printDeck(const std::array<Card,52> &deck)
{
	for (const auto &card : deck)
	{
		printCard(card	);
		std::cout << " ";
	}
}

void swapCard(Card &card1, Card &card2)
{
	Card tempcard = card1;
	card1 = card2;
	card2 = tempcard;
}

void shuffleDeck(std::array<Card,52> &deck)
{
	std::uniform_int_distribution<> shuffle(1,52); //Generator
	for (auto &card : deck)
		swapCard(card,deck[shuffle(Rand::mersenne)-1]);
}

int getCardValue(Card card)
{
	switch(card.rank)
	{
	case TWO: return 2;
	case THREE: return 3;
	case FOUR: return 4;
	case FIVE: return 5;
	case SIX: return 6;
	case SEVEN: return 7;
	case EIGHT: return 8;
	case NINE: return 9;
	case TEN:
	case JACK:
	case QUEEN:
	case KING:
		return 10;
	case ACE: return 11;
	}
}

int playBlackjack(const std::array<Card,52> &deck) // -1 if player loses, 0 if there's a tie and 1 if the player wins.
{
	//initialization
	const Card *cardptr = &deck[0];
	int dealerScore {};
	int playerScore {};
	//Blackjack
	std::cout << "The dealer draws one card:\n";
	dealerScore += getCardValue(*cardptr++);
	std::cout << "The dealer has " << dealerScore << " points.\n";
	std::cout << "The player draws two cards:\n";
	playerScore += getCardValue(*cardptr++);
	playerScore += getCardValue(*cardptr++);
	std::cout << "The player has " << playerScore << " points.\n";
	char decision{};
	while(true)
	{
		if (playerScore == 21)
		{
			std::cout << "You win!\n";
			return 1;
		}
		do
		{
			std::cout << "Do you want to \"hit\" (h) or \"stand\" (s)? ";
			std::cin >> decision;
		}
		while(decision != 's' && decision != 'h');

		if(decision == 'h')
		{
			std::cout << "The player draws one card:\n";
			if(getCardValue(*cardptr) == 11) // This takes in account if the Ace points 1 or 11.
			{
				playerScore += getCardValue(*cardptr++);
				if (playerScore > 21)
					playerScore -= 10;
			}
			else
				playerScore +=getCardValue(*cardptr++);

			std::cout << "The player has " << playerScore << " points.\n";
			if (playerScore > 21)
			{
				std::cout << "Busted. You lose!\n";
				return -1;
			}
			else
				continue;
		}
		else //This else is for stand
		{
			std::cout << "You stand.\n";
			break;
		}
	}
	std:: cout << "Now it is the turn of the dealer.\n";
	while(true)
	{
		std::cout << "The dealer draws one card:\n";
		if(getCardValue(*cardptr) == 11)
		{
			dealerScore += getCardValue(*cardptr++);
			if(dealerScore > 21)
				dealerScore -= 10;
		}
		else
			dealerScore += getCardValue(*cardptr++);

		std::cout << "The dealer has " << dealerScore << " points.\n";
		if (dealerScore < 17)
			continue;
		else if (dealerScore > 21)
		{
			std::cout << "The dealer busts. You win!";
			return 1;
		}
		else
			break;
	}
	//Last comparison
	if (playerScore > dealerScore)
	{
		std::cout << "Your score is greater than the dealer's score. You win!";
		return 1;
	}
	else if (playerScore == dealerScore)
	{
		std::cout << "The player and the dealer have the same score. The result is a tie!";
		return 0;
	}
	else
	{
		std::cout <<"Your score is lesser than the dealer's score. You lose!";
		return -1;
	}




}
int main()
{
	std::array<Card,52> deck; //Initialize deck
	for (int i{0}; i<MAXSUITS; ++i)
		for (int j{0}; j<MAXRANKS; ++j)
		{
			deck[j+i*MAXRANKS].rank = static_cast<Rank>(j);
			deck[j+i*MAXRANKS].suits = static_cast<Suits>(i);
		}
	shuffleDeck(deck);
	playBlackjack(deck);
	return 0;
}
