#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Coordinates.hpp"

class Player
{
    Coordinates coord;
    int health;

public:
        Player();
        virtual ~Player();

};

#endif // PLAYER_HPP
