#include <SFML/Graphics.hpp>
#include <iostream>
#include <thread>

#include "Coordinates.hpp"

int main()
{
    sf::RenderWindow window(sf::VideoMode(300,300), "Window");
    window.setFramerateLimit(60);
    Coordinates coord;
    sf::Font font;
    font.loadFromFile("resources/vgafix.fon");

    while(window.isOpen())
    {
        sf::Event event;
        while(window.pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                {
                    window.close();
                    break;
                }
                // Coordinates movement
                case sf::Event::KeyPressed:
                {
                    if (event.key.code == sf::Keyboard::Left)
                    {
                        coord.addX(-1);
                        coord.printCoord();
                    }
                    if (event.key.code == sf::Keyboard::Right)
                    {
                        coord.addX(1);
                        coord.printCoord();
                    }
                    if (event.key.code == sf::Keyboard::Up)
                    {
                        coord.addY(1);
                        coord.printCoord();
                    }
                    if (event.key.code == sf::Keyboard::Down)
                    {
                        coord.addY(-1);
                        coord.printCoord();
                    }
                    if (event.key.code == sf::Keyboard::Escape)
                    {
                        window.close();
                        break;
                    }
                }
                default:
                    break;
            }

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
            {
                std::cout << "Pew! ";
                // This vector is used to update the event when the mouse is not moving.
                sf::Vector2i v = sf::Mouse::getPosition(window);
                sf::Mouse::setPosition(v, window);
            }
        }

        window.clear(sf::Color::Black);

        sf::Text text;
        text.setFont(font)
        text.setString("Bruh");

        window.display();
    }

    return 0;
}
