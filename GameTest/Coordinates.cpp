#include <iostream>

#include "Coordinates.hpp"

Coordinates::Coordinates(int x, int y)
    : m_x(x), m_y(y)
{
}

int Coordinates::getX()
{
    return m_x;
}

int Coordinates::getY()
{
    return m_y;
}

void Coordinates::addX(int x)
{
    m_x += x;
}

void Coordinates::addY(int y)
{
    m_y += y;
}

void Coordinates::printCoord()
{
    std::cout << "(" << m_x << ", " << m_y << ")\n";
}
