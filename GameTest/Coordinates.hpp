#ifndef COORDINATES_HPP_INCLUDED
#define COORDINATES_HPP_INCLUDED

class Coordinates
{
    int m_x{};
    int m_y{};

public:
    Coordinates() = default;
    Coordinates(int x, int y);

    int getX();
    int getY();

    void addX(int x);
    void addY(int y);

    void printCoord();
};

#endif // COORDINATES_HPP_INCLUDED
