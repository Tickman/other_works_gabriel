#include <cmath>
#include <string>
#include <iostream>

int factorial(int N)
{
    if (N <= 1)
        return 1;
    else
        return N*factorial(N-1);

}

int sumDigits(int number)
{
    if (number <= 1)
        return 0;
    int length = std::to_string(number).length();
    int small_num = number/std::pow(10,length-1);
    return small_num + sumDigits(number-small_num*std::pow(10,length-1));
}

void decimalToBinary(int num)
{
    if (num == 0)
        return;
    decimalToBinary(num / 2);
    std::cout << num % 2;
}
