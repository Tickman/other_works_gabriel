#ifndef STUDENTS_H_INCLUDED
#define STUDENTS_H_INCLUDED

#include <string>

namespace students
{
    struct Student
    {
        std::string name;
        int score;
    };

void student_grade(Student &student);
bool great(Student st1, Student st2);
void print_both(Student st);

}

#endif // STUDENTS_H_INCLUDED
