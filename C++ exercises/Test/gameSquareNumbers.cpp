#include <algorithm>
#include <vector>
#include <iostream>
#include <cmath>
#include <random>
#include <ctime>


void gameSquareNumbers()
{
    std::cout << "Start where? ";
    int start;
    std::cin >> start;
    std::cout << "How many? ";
    int amount;
    std::cin >> amount;

    std::mt19937 mersenne{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };
    std::uniform_int_distribution randomizer{2, 4};
    int random_number = randomizer(mersenne);

    std::vector<int> numbers(amount);
    for (int i=0; i != amount; ++i)
    {
        numbers[i] = (start+i)*(start+i)*random_number;
    }

    std::cout << "I generated " << amount << " square numbers. Do you know what each number is after multiplying it by " << random_number <<"?\n";

    for (int i=amount; i != 0; --i)
    {
        int guess;
        std::cin >> guess;
        auto found{std::find(numbers.begin(), numbers.end(), guess)};
        if (found != numbers.end())
        {
            if (i == 1)
            {
                std::cout << "Nice! You found all numbers, good job!";
                break;
            }
            std::cout << "Nice! " << i-1 << " number(s) left.\n";
            numbers.erase(found);
        }
        else
        {
            auto smallest{std::min_element(numbers.begin(), numbers.end())};
            auto small_difference{[=]() { return std::abs(*smallest-guess) < 4; }};
            if (small_difference())
            {
                std::cout << guess << " is wrong! Try " << *smallest << " next time.";
                break;
            }
            else
            {
                std::cout << guess << " is wrong!";
                break;
            }
        }
    }
}
