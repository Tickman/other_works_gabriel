#include <iostream>
#include <string>
#include <random>
#include <ctime>
#include <vector>

#include "classes.h"

/*
// Items.h

using namespace items;

int main()
{
    std::array<int, static_cast<int>(Item_Type::MAX_ITEMS)> pouch{};
    pouch[static_cast<int>(Item_Type::HEALTH_POTION)] = 2;
    pouch[static_cast<int>(Item_Type::TORCH)] = 5;
    pouch[static_cast<int>(Item_Type::ARROW)] = 10;
    std::cout << "You have a total of " << countTotalItems(pouch) << " items and " << pouch[static_cast<int>(Item_Type::TORCH)] << " torches.";

    return 0;
}
*/


//-----------------------------------------------------------------------


/*
// students.h

using namespace students;

int main()
{
    std::cout << "How many students are? ";
    int num_students;
    std::cin >> num_students;

    std::vector<Student> students(num_students);
    std::for_each(students.begin(), students.end(), student_grade);
    std::sort(students.begin(), students.end(), great);

    for_each(students.begin(), students.end(), print_both);
    return 0;
}
*/


//---------------------------------------------------------------

/*
// operate.h

int main()
{
    int num1;
    int num2;
    char op;
    get_input(num1, num2, op);
    arithmeticFcn fcn{getArithmeticFunction(op)};
    std::cout << num1 << op << num2 << "=" << fcn(num1,num2);

    return 0;
}
*/

//--------------------------------------------------------------------

/*
// students.h

using namespace students;

int main()
{
    std::array<Student, 8> arr{
      { { "Albert", 3 },
        { "Ben", 5 },
        { "Christine", 2 },
        { "Dan", 8 }, // Dan has the most points (8).
        { "Enchilada", 4 },
        { "Francis", 1 },
        { "Greg", 3 },
        { "Hagrid", 5 } }
    };

    auto find_max_score{ [](Student st1, Student st2) -> bool
    {
        return st1.score < st2.score;
    }
    };

    auto st = std::max_element(arr.begin(), arr.end(), find_max_score);

    std::cout << (*st).name << " is the best student.";

    return 0;
}
*/

//----------------------------------------------------------------------


/*
// binarySearch.h
int main()
{
    constexpr int array[]{ 3, 6, 8, 12, 14, 17, 20, 21, 26, 32, 36, 37, 42, 44, 48 };

    // We're going to test a bunch of values to see if they produce the expected results
    constexpr int numTestValues{ 9 };
    // Here are the test values
    constexpr int testValues[numTestValues]{ 0, 3, 12, 13, 22, 26, 43, 44, 49 };
    // And here are the expected results for each value
    int expectedValues[numTestValues]{ -1, 0, 3, -1, -1, 8, -1, 13, -1 };

    // Loop through all of the test values
    for (int count{ 0 }; count < numTestValues; ++count)
    {
        // See if our test value is in the array
        int index{ iterative_binarySearch(array, testValues[count], 0, 14) };
        // If it matches our expected value, then great!
        if (index == expectedValues[count])
             std::cout << "test value " << testValues[count] << " passed!\n";
        else // otherwise, our binarySearch() function must be broken
             std::cout << "test value " << testValues[count] << " failed.  There's something wrong with your code!\n";
    }

    return 0;
}
*/

/*
//Blackjack Game with classes

bool playerWantsHit()
{
    while (true)
    {
        std::cout << "(h) to hit, or (s) to stand: ";

        char ch{};
        std::cin >> ch;

    switch (ch)
        {
        case 'h':
        return true;
        case 's':
        return false;
        }
    }
}

// Returns true if the player went bust. False otherwise.
bool playerTurn(Deck& deck, Player& player)
{
    while (true)
    {
        std::cout << "You have: " << player.score() << '\n';

        if (player.isBust())
        {
            return true;
        }
        else
        {
            if (playerWantsHit())
            {
                player.drawCard(deck);
            }
            else
            {
            // The player didn't go bust.
                return false;
            }
        }
    }
}

// Returns true if the dealer went bust. False otherwise.
bool dealerTurn(Deck& deck, Player& dealer)
{
    while (dealer.score() < minimumDealerScore)
    {
        dealer.drawCard(deck);
        std::cout << "The dealer has " << dealer.score() << ".\n";
    }

    return (dealer.score() > maximumScore);
}

bool playBlackjack(Deck& deck)
{
    Player player{};
    Player dealer{};

    dealer.drawCard(deck);

    std::cout << "The dealer is showing: " << dealer.score() << '\n';

    player.drawCard(deck).drawCard(deck);

    if (playerTurn(deck, player))
    {
        return false;
    }

    if (dealerTurn(deck, dealer))
    {
        return true;
    }

    return (player.score() > dealer.score());
}

int main()
{
    Deck deck{};
    deck.shuffle();

    if (playBlackjack(deck))
    {
        std::cout << "You win!\n";
    }
    else
    {
        std::cout << "You lose!\n";
    }

    return 0;
}
*/



int main()
{
    int num;
    int den;
	std::cout << "Enter the numerator: ";
	std::cin >> num;
	std::cout << "Enter the denominator: ";
	std::cin >> den;
	Fraction f{num, den};
	f.print();

	return 0;
}
