#include <iostream>
#include <functional>

void get_input(int &num1, int &num2, char &op)
{
    std::cout << "Enter two integers:\n";
    std::cin >> num1;
    std::cin >> num2;
    do
    {
        std::cout << "Enter an operator: ";
        std::cin >> op;
    }
    while(op != '+' && op != '-' && op != '*' && op != '/');

}

int add(int num1, int num2)
{
    return num1+num2;
}

int substract(int num1, int num2)
{
    return num1-num2;
}

int multiply(int num1, int num2)
{
    return num1*num2;
}

int divide(int num1, int num2)
{
    return num1/num2;
}

using arithmeticFcn = std::function<int(int ,int)>;
arithmeticFcn getArithmeticFunction(char op)
{
    switch (op)
	{
	case '+': return add;
	case '-': return substract;
	case '*': return multiply;
	case '/': return divide;
	default: return nullptr;
	}
}
