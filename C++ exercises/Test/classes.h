#ifndef CLASSES_H_INCLUDED
#define CLASSES_H_INCLUDED

#include <array>
#include <vector>
#include <chrono>
#include <string>
#include <random>
#include <initializer_list>


class Point3d
{
    int m_x;
    int m_y;
    int m_z;

public:
    void setValues(int x, int y, int z);
    void print();
    bool isEqual(Point3d &point2);

};


class Stack
{
    std::array<int, 10> m_array{};
    int m_size = 0;

public:
    void reset();
    bool push(int number);
    void pop();
    void print();

};


class Ball
{
    std::string m_color{"black"};
    double m_radius{10.0};

public:
    Ball();
    Ball(std::string color, double radius),
    Ball(std::string color);
    Ball(double radius);
    void print();
};


class RGBA
{
    using intf = std::uint_fast8_t;
    intf m_red;
    intf m_green;
    intf m_blue;
    intf m_alpha;

public:
    RGBA(intf red , intf green, intf blue, intf alpha);
    void print();
};

class Vector3d;

class Point3D
{
private:
	double m_x, m_y, m_z;

public:
	Point3D(double x = 0.0, double y = 0.0, double z = 0.0);
	void print();
	void moveByVector(const Vector3d &v);
};

class Vector3d
{
private:
	double m_x, m_y, m_z;

public:
	Vector3d(double x = 0.0, double y = 0.0, double z = 0.0);
	void print();
	friend void Point3D::moveByVector(const Vector3d &v);
};

class Timer
{
private:
	// Type aliases to make accessing nested type easier
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer();
	void reset();
	double elapsed() const;
};

class Monster
{
    int m_type;
    std::string m_name;
    std::string m_roar;
    int m_hp;

    std::string getTypeString(int type);

public:

    enum MonsterType
    {
        DRAGON,
        GOBLIN,
        OGRE,
        ORC,
        SKELETON,
        TROLL,
        VAMPIRE,
        ZOMBIE,
        MAX_MONSTER_TYPES,
    };

    Monster(int type, std::string name, std::string roar, int hp);
    void print();
};

class MonsterGenerator
{
public:

    static Monster generateMonster();
    static int getRandomNumber(int min, int max);
};


//Classes for the Blackjack

// Maximum score before losing.
constexpr int maximumScore{ 21 };

// Minium score that the dealer has to have.
constexpr int minimumDealerScore{ 17 };

enum class CardSuit
{
    SUIT_CLUB,
    SUIT_DIAMOND,
    SUIT_HEART,
    SUIT_SPADE,

    MAX_SUITS
};

enum class CardRank
{
    RANK_2,
    RANK_3,
    RANK_4,
    RANK_5,
    RANK_6,
    RANK_7,
    RANK_8,
    RANK_9,
    RANK_10,
    RANK_JACK,
    RANK_QUEEN,
    RANK_KING,
    RANK_ACE,

    MAX_RANKS
};

class Card
{
    CardRank m_rank{};
    CardSuit m_suit{};

public:

    Card();
    Card(CardRank, CardSuit);
    void print() const;
    int value() const;
};

using deck_type = std::array<Card, 52>;
using index_type = deck_type::size_type;

class Deck
{
    deck_type m_deck;
    int m_cardIndex{0};

public:

    Deck();
    void print();
    void shuffle();
    const Card& dealCard();
};

class Player
{
    int m_score;

public:
    Player& drawCard(Deck& deck);
    int score();
    bool isBust();
};


class Point
{
private:
    double m_x, m_y, m_z;

public:
    Point(double x=0.0, double y=0.0, double z=0.0):
        m_x(x), m_y(y), m_z(z)
    {
    }

    // Convert a Point into its negative equivalent
    Point operator- () const;

    // Return true if the point is set at the origin
    bool operator! () const;

    //Returns a point
    Point operator+ () const;

    double getX() const { return m_x; }
    double getY() const { return m_y; }
    double getZ() const { return m_z; }
};

//Fraction class
class Fraction
{
    int m_numerator{0};
    int m_denominator{1};

public:

    Fraction(int n, int d);
    Fraction() = default;

    void print();

    int gcd(int a, int b);

    void reduce();

    friend Fraction operator*(const Fraction& f1, const Fraction& f2);
    friend Fraction operator*(const Fraction& f1, const int i);
    friend Fraction operator*(const int i, const Fraction& f2);
    friend std::ostream& operator<< (std::ostream &out, const Fraction &f);
    friend std::istream& operator>> (std::istream &in, Fraction &f);
};

//Cents class
class Cents
{
private:
    int m_cents;

public:
    Cents(int cents);

    friend bool operator> (const Cents &c1, const Cents &c2);
    friend bool operator<= (const Cents &c1, const Cents &c2);

    friend bool operator< (const Cents &c1, const Cents &c2);
    friend bool operator>= (const Cents &c1, const Cents &c2);
};

//Average class

class Average
{
    int32_t m_totalSum{0};
    int8_t m_totalNumbers{0};

public:
    Average& operator+= (int num);
    friend std::ostream& operator<< (std::ostream& out, Average& avg);
};

//IntArray class
class IntArray
{
    int m_length{};
    int *m_array{nullptr};

public:
    IntArray(int length);
    IntArray(const IntArray& b);

    friend std::ostream& operator<< (std::ostream& out, const IntArray& a);

    int& operator[] (int index);
    IntArray& operator= (const IntArray& a);

    ~IntArray();
};

//FixedPoint2 class
class FixedPoint2
{
    int_least16_t m_noFrac{};
    int_least8_t m_frac{};

public:
    FixedPoint2(int noFrac, int frac);
    FixedPoint2(double num);

    friend std::ostream& operator<< (std::ostream& out, const FixedPoint2& num);
    operator double() const;

    friend bool operator== (FixedPoint2& num1, FixedPoint2& num2);
    friend std::istream& operator>> (std::istream& in, FixedPoint2& num);
    friend FixedPoint2 operator+ (FixedPoint2& num1, FixedPoint2& num2);

    FixedPoint2 operator- (FixedPoint2& num);
};


// Inheritance
//Fruit class
class Fruit
{
protected:
    std::string m_name{};
    std::string m_color{};

public:
    Fruit(const std::string name, const std::string color);

    const std::string& get_name() const;
    const std::string& get_color() const;
};

class Apple : public Fruit
{
    double m_fiber{};

protected:
    Apple(const std::string& name, const std::string& color);

public:
    Apple(const std::string& name, const std::string& color, const double& fiber);
    Apple(const std::string &color);


    friend std::ostream& operator<< (std::ostream& out, const Apple& apple);
    const double get_fiber() const;
};

class Banana : public Fruit
{
public:
    Banana(const std::string& name, const std::string& color);
    Banana();

    friend std::ostream& operator<< (std::ostream& out, const Banana& banana);
};

class GrannySmith : public Apple
{
public:
    GrannySmith();
};

// Shape class
class Shape
{

public:
    virtual std::ostream& print(std::ostream& out) const = 0;
    friend std::ostream& operator<< (std::ostream& out, const Shape& shape);
    virtual ~Shape() {}
};


class IntPoint
{
private:
	int m_x = 0;
	int m_y = 0;
	int m_z = 0;

public:
	IntPoint(int x, int y, int z);

	friend std::ostream& operator<<(std::ostream &out, const IntPoint &p);
};

// Triangle class
class Triangle : public Shape
{
    const IntPoint m_points[3];

public:
    Triangle(IntPoint p1, IntPoint p2, IntPoint p3);
    virtual std::ostream& print(std::ostream& out) const;
    virtual ~Triangle() {}

};

// Circle class
class Circle : public Shape
{
    IntPoint m_center;
    int m_radius{};

public:
    Circle(IntPoint p, int radius);
    virtual std::ostream& print(std::ostream& out) const;
    virtual ~Circle() {}
    int& getRadius();
};

int getLargestRadius(std::vector<Shape*> &v);


// Fraction class

// Templates

template<class T>
class Pair1
{
    T m_first{};
    T m_second{};

public:
    Pair1(T first, T second)
        : m_first(first), m_second(second)
    {
    }

    const T& first() const
    {
        return m_first;
    }

    const T& second() const
    {
        return m_second;
    }
};

template<class T1, class T2>
class Pair
{
    T1 m_first{};
    T2 m_second{};

public:
    Pair(T1 first, T2 second)
        : m_first(first), m_second(second)
    {
    }

    const T1& first() const
    {
        return m_first;
    }

    const T2& second() const
    {
        return m_second;
    }
};

template<class S>
class StringValuePair : public Pair<std::string, S>
{
public:
    StringValuePair(std::string first, S second)
        : Pair<std::string, S>(first, second)
    {
    }
};



#endif // CLASSES_H_INCLUDED
