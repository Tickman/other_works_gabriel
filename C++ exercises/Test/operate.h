#ifndef OPERATE_H_INCLUDED
#define OPERATE_H_INCLUDED

#include <functional>

void get_input(int &num1, int &num2, char &op);
int add(int num1, int num2);
int substract(int num1, int num2);
int multiply(int num1, int num2);
int divide(int num1, int num2);

using arithmeticFcn = std::function<int(int ,int)>;
arithmeticFcn getArithmeticFunction(char op);


#endif // OPERATE_H_INCLUDED
