#include <iostream>
#include "students.h"
namespace students
{
    void student_grade(Student &student)
    {
        std::cin >> student.name;
        std::cin >> student.score;
    }

    bool great(Student st1, Student st2)
    {
        return (st1.score > st2.score);
    }

    void print_both(Student st)
    {
        std::cout << st.name << " got a grade of " << st.score << ".\n";
    }
}
