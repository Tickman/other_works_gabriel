#include <cmath>


int recursive_binarySearch(const int *array, int target, int min, int max)
{
    int length = max-min;
    int middle_index = min+static_cast<int>(std::ceil(length/2.0));
    int array_value = array[middle_index];
    if (max < min)
        return -1;
    else if (target < array_value)
        return recursive_binarySearch(array, target, min, middle_index-1);
    else if (target > array_value)
        return recursive_binarySearch(array, target, middle_index+1, max);
    else
        return middle_index;
}

int iterative_binarySearch(const int *array, int target, int min, int max)
{
    while(1)
    {
        int length = max-min;
        int middle_index = min+static_cast<int>(std::ceil(length/2.0));
        int array_value = array[middle_index];
        if (max < min)
            return -1;
        else if (target < array_value)
        {
            max = middle_index-1;
            continue;
        }
        else if (target > array_value)
        {
            min = middle_index+1;
            continue;
        }
        else
            return middle_index;
    }
}
