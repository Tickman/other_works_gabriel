#include "classes.h"

#include <iostream>
#include <cassert>
#include <array>
#include <vector>
#include <random>
#include <algorithm>
#include <cmath>
#include <initializer_list>
#include <exception>

//Point3d
void Point3d::setValues(int x, int y, int z)
{
    m_x = x;
    m_y = y;
    m_z = z;
}

void Point3d::print()
{
    std::cout << "<" << m_x << ", " << m_y << ", " << m_z << ">\n";
}

bool Point3d::isEqual(Point3d &point2)
{
    return m_x == point2.m_x && m_y == point2.m_y && m_z == point2.m_z;
}

//Stack
void Stack::reset()
{
    m_size = 0;
}

bool Stack::push(int number)
{
    if(static_cast<unsigned int>(m_size) == m_array.size())
        return false;
    else
    {
        m_array[m_size++] = number;
        return true;
    }

}

void Stack::pop()
{
    assert(m_size != 0);
    m_array[--m_size] = 0;
}

void Stack::print()
{
    std::cout << "( ";
    for (int i = 0; i != m_size; i++)
    {
        std::cout << m_array[i] << " ";
    }
    std::cout << ")\n";
}


//Ball
Ball::Ball() = default;

Ball::Ball(std::string color, double radius) : m_color(color), m_radius(radius)
{
}

Ball::Ball(std::string color) : m_color(color)
{
}

Ball::Ball(double radius) : m_radius(radius)
{
}

void Ball::print()
{
    std::cout << "color:" << m_color << ", radius: " << m_radius << "\n";
}


//RGBA
using intf = std::uint_fast8_t;

RGBA::RGBA(intf red = 0, intf green = 0, intf blue = 0, intf alpha = 255)
    : m_red(red), m_green(green), m_blue(blue), m_alpha(alpha)
{
}

void RGBA::print()
{
    std::cout << "r=" << static_cast<int>(m_red) << " g=" << static_cast<int>(m_green)
    << " b=" << static_cast<int>(m_blue) << " a=" << static_cast<int>(m_alpha);
}


//Vector3d
Vector3d::Vector3d(double x, double y, double z)
    : m_x(x), m_y(y), m_z(z)
{
}

void Vector3d::print()
{
    std::cout << "Vector(" << m_x << " , " << m_y << " , " << m_z << ")\n";
}

//Point3D
Point3D::Point3D(double x, double y, double z)
		: m_x(x), m_y(y), m_z(z)
{
}

void Point3D::print()
{
    std::cout << "Point(" << m_x << " , " << m_y << " , " << m_z << ")\n";
}

void Point3D::moveByVector(const Vector3d &v)
{
    m_x += v.m_x;
    m_y += v.m_y;
    m_z += v.m_z;
}


//Timer

Timer::Timer() : m_beg(clock_t::now())
{
}

void Timer::reset()
{
    m_beg = clock_t::now();
}

double Timer::elapsed() const
{
    return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
}


//Monster
Monster::Monster(int type, std::string name, std::string roar, int hp)
                : m_type(type), m_name(name), m_roar(roar), m_hp(hp)
{
}

std::string Monster::getTypeString(int type)
{
    switch(type)
    {
    case 0:
        return "dragon";
    case 1:
        return "goblin";
    case 2:
        return "ogre";
    case 3:
        return "orc";
    case 4:
        return "skeleton";
    case 5:
        return "troll";
    case 6:
        return "vampire";
    case 7:
        return "zombie";
    default:
        return "Type_Not_Found";
    }
}


void Monster::print()
{
    std::cout << m_name << " the " << getTypeString(m_type) << " has " << m_hp << " hit points and says " << m_roar << "\n";
}


//MonsterGenerator
Monster MonsterGenerator::generateMonster()
{
    int type = MonsterGenerator::getRandomNumber(0, Monster::MAX_MONSTER_TYPES-1);
    int hp = getRandomNumber(1, 100);
    static std::array<std::string,6> s_names = {"Jimmy", "Belaphor", "Destroyer", "Wannabe", "Colt", "Remedy"};
    static std::array<std::string,6> s_roars = {"*pickle*", "*yawn*", "*bark*", "*squeak*", "*purr*", "*growl*"};
    std::string name = s_names[getRandomNumber(0,s_names.size()-1)];
    std::string roar = s_roars[getRandomNumber(0,s_roars.size()-1)];
    return Monster(type, name, roar, hp);
}

static std::mt19937 mersenne{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };

int MonsterGenerator::getRandomNumber(int min, int max)
{
    std::uniform_int_distribution die{ min, max };
    return die(mersenne);
}


//Methods for BlackJack

//Card
Card::Card() = default;
Card::Card(CardRank rank, CardSuit suit) : m_rank(rank), m_suit(suit)
{
}

void Card::print() const
{
    switch (m_rank)
    {
    case CardRank::RANK_2:
        std::cout << '2';
        break;
    case CardRank::RANK_3:
        std::cout << '3';
        break;
    case CardRank::RANK_4:
        std::cout << '4';
        break;
    case CardRank::RANK_5:
        std::cout << '5';
        break;
    case CardRank::RANK_6:
        std::cout << '6';
        break;
    case CardRank::RANK_7:
        std::cout << '7';
        break;
    case CardRank::RANK_8:
        std::cout << '8';
        break;
    case CardRank::RANK_9:
        std::cout << '9';
        break;
    case CardRank::RANK_10:
        std::cout << 'T';
        break;
    case CardRank::RANK_JACK:
        std::cout << 'J';
        break;
    case CardRank::RANK_QUEEN:
        std::cout << 'Q';
        break;
    case CardRank::RANK_KING:
        std::cout << 'K';
        break;
    case CardRank::RANK_ACE:
        std::cout << 'A';
        break;
    default:
        std::cout << '?';
        break;
    }

    switch (m_suit)
    {
    case CardSuit::SUIT_CLUB:
        std::cout << 'C';
        break;
    case CardSuit::SUIT_DIAMOND:
        std::cout << 'D';
        break;
    case CardSuit::SUIT_HEART:
        std::cout << 'H';
        break;
    case CardSuit::SUIT_SPADE:
        std::cout << 'S';
        break;
    default:
        std::cout << '?';
        break;
  }
}

int Card::value() const
{
    if (m_rank <= CardRank::RANK_10)
    {
        return (static_cast<int>(m_rank) + 2);
    }

    switch (m_rank)
    {
    case CardRank::RANK_JACK:
    case CardRank::RANK_QUEEN:
    case CardRank::RANK_KING:
        return 10;
    case CardRank::RANK_ACE:
        return 11;
    default:
        assert(false && "should never happen");
        return 0;
    }
}

//Deck

Deck::Deck()
{
    index_type card{ 0 };

    auto suits{ static_cast<index_type>(CardSuit::MAX_SUITS) };
    auto ranks{ static_cast<index_type>(CardRank::MAX_RANKS) };

    for (index_type suit{ 0 }; suit < suits; ++suit)
    {
        for (index_type rank{ 0 }; rank < ranks; ++rank)
        {
            m_deck[card] = { static_cast<CardRank>(rank), static_cast<CardSuit>(suit) };
            ++card;
        }
    }
}

void Deck::print()
{
    for (const auto& card : m_deck)
    {
        card.print();
        std::cout << ' ';
    }

    std::cout << '\n';
}

void Deck::shuffle()
{
    static std::mt19937 mt{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };
    m_cardIndex = 0;
    std::shuffle(m_deck.begin(), m_deck.end(), mt);
}

const Card& Deck::dealCard()
{
    return m_deck[m_cardIndex++];
}

//player


Player& Player::drawCard(Deck& deck)
{
    m_score += deck.dealCard().value();
    return *this;
}

int Player::score()
{
    return m_score;
}

bool Player::isBust()
{
    return m_score > maximumScore;
}

//Point methods
// Convert a Point into its negative equivalent
Point Point::operator- () const
{
    return Point(-m_x, -m_y, -m_z);
}

// Return true if the point is set at the origin, false otherwise
bool Point::operator! () const
{
    return (m_x == 0.0 && m_y == 0.0 && m_z == 0.0);
}

Point Point::operator+() const
{
    return *this;
}

//Fraction methods
Fraction::Fraction(int n, int d) : m_numerator(n), m_denominator(d)
{
    reduce();
}

void Fraction::print()
{
    try
    {
        if (m_denominator == 0)
            throw std::runtime_error("Invalid denominator");
        std::cout << m_numerator << "/" << m_denominator << "\n";
    }
    catch (std::exception &exception)
    {
        std::cout << exception.what();
    }

}

int Fraction::gcd(int a, int b)
{
    return (b == 0) ? a : gcd(b, a % b);
}

void Fraction::reduce()
{
    int gcd = Fraction::gcd(m_numerator, m_denominator);
    m_numerator /= gcd;
    m_denominator /= gcd;
}

Fraction operator*(const Fraction& f1, const Fraction& f2)
{
    return Fraction(f1.m_numerator*f2.m_numerator, f1.m_denominator*f2.m_denominator);
}

Fraction operator*(const Fraction& f1, const int i)
{
    return Fraction(f1.m_numerator*i, f1.m_denominator);
}

Fraction operator*(const int i, const Fraction& f2)
{
    return f2*i;
}

std::ostream& operator<< (std::ostream &out, const Fraction &f)
{
    out << f.m_numerator << "/" << f.m_denominator;
    return out;
}

std::istream& operator>> (std::istream &in, Fraction &f)
{
    in >> f.m_numerator;
    in.ignore(1);
    in >> f.m_denominator;
    return in;
}

//Cents methods
//Cents class
Cents::Cents(int cents): m_cents{ cents }
{
}

bool operator> (const Cents &c1, const Cents &c2)
{
    return c1.m_cents > c2.m_cents;
}

bool operator>= (const Cents &c1, const Cents &c2)
{
    return c1.m_cents >= c2.m_cents;
}

bool operator< (const Cents &c1, const Cents &c2)
{
    return !(c1 >= c2);
}

bool operator<= (const Cents &c1, const Cents &c2)
{
    return !(c1 > c2);
}

//Average methods
Average& Average::operator+=(int num)
{
    m_totalSum += num;
    //Problem. Does not chain correctly because of this. Don't know why it happens, though.
    ++m_totalNumbers;
    return *this;
}

std::ostream& operator<< (std::ostream& out, Average& avg)
{
    out << static_cast<double>(avg.m_totalSum) / avg.m_totalNumbers;
    return out;
}

//IntArray methods
IntArray::IntArray(int length) : m_length(length)
{
    assert (length > 0);
    m_array = new int[length];
}

IntArray::IntArray(const IntArray& b) : m_length(b.m_length)
{
    m_array = new int[b.m_length];
    for (int i{0}; i != b.m_length; i++)
        m_array[i] = b.m_array[i];
}

IntArray::~IntArray()
{
    delete[] m_array;
}

std::ostream& operator<< (std::ostream& out, const IntArray& a)
{
    for(int i{0}; i != a.m_length; i++)
        out << a.m_array[i] << " ";
    return out;
}

int& IntArray::operator[] (const int index)
{
    assert(index >= 0);
    assert(index < m_length);
    return m_array[index];
}

IntArray& IntArray::operator=(const IntArray& a)
{
    if (this == &a)
        return *this;

    delete[] m_array;

    m_length = a.m_length;
    m_array = new int[a.m_length];
    for (int i{0}; i != a.m_length; i++)
            m_array[i] = a.m_array[i];

    return *this;
}

//FixedPoint2 methods

FixedPoint2::FixedPoint2(int noFrac, int frac) : m_noFrac(noFrac), m_frac(frac)
{
    assert (-99 < frac && frac < 99);
}

FixedPoint2::FixedPoint2(double num)
{
    m_noFrac = static_cast<int_least16_t>(num);
    m_frac = static_cast<int_least8_t>(std::round((num-m_noFrac)*100));
}

std::ostream& operator<< (std::ostream& out, const FixedPoint2& num)
{
    if (std::abs(num.m_frac) >= 10)
        if (num.m_frac < 0)
            out << "-" << std::abs(num.m_noFrac) << "." << -static_cast<int>(num.m_frac);
        else
            out << num.m_noFrac << "." << static_cast<int>(num.m_frac);
    else
        if (num.m_frac < 0)
            out << "-" << std::abs(num.m_noFrac) << "." << 0 << -static_cast<int>(num.m_frac);
        else
            out << num.m_noFrac << "." << 0 << static_cast<int>(num.m_frac);
    return out;
}

FixedPoint2::operator double() const
{
    if (m_frac < 0)
        return -std::abs(m_noFrac) + (m_frac)/100.0;
    else
        return m_noFrac + m_frac/100.0;
}

bool operator== (FixedPoint2& num1, FixedPoint2& num2)
{
    return static_cast<double>(num1) == static_cast<double>(num2);
}

std::istream& operator>> (std::istream& in, FixedPoint2& num)
{
    double number{};
    in >> number;
    num = FixedPoint2(number);
    return in;
}

FixedPoint2 operator+ (FixedPoint2& num1, FixedPoint2& num2)
{
    double sum = static_cast<double>(num1) + static_cast<double>(num2);
    return FixedPoint2(sum);
}

FixedPoint2 FixedPoint2::operator- (FixedPoint2& num)
{
    double number = -static_cast<double>(num);
    return FixedPoint2(number);
}

//Fruits methods
Fruit::Fruit(const std::string name, const std::string color)
        : m_name(name), m_color(color)
{
}

const std::string& Fruit::get_name() const
{
    return m_name;
}

const std::string& Fruit::get_color() const
{
    return m_color;
}

Apple::Apple(const std::string& name, const std::string& color, const double& fiber)
        : Fruit{name, color}, m_fiber{fiber}
{
}

Apple::Apple(const std::string& name, const std::string& color)
        : Fruit(name, color)
{
}

Apple::Apple(const std::string &color)
        : Fruit{"apple", color}
{
}

std::ostream& operator<< (std::ostream& out, const Apple& apple)
{
    out << "Apple(" << apple.get_name() << ", " << apple.get_color() << ", " << apple.get_fiber() << ")";
    return out;
}

const double Apple::get_fiber() const { return m_fiber; }

Banana::Banana(const std::string& name, const std::string& color)
        : Fruit{name, color}
{
}

Banana::Banana() : Fruit{"banana", "yellow"} {}

std::ostream& operator<< (std::ostream& out, const Banana& banana)
{
    out << "Banana(" << banana.get_name() << ", " << banana.get_color() << ")";
    return out;
}

GrannySmith::GrannySmith() : Apple("granny smith", "green")
{
}


// Shape methods
std::ostream& operator<< (std::ostream& out, const Shape& shape)
{
    return shape.print(out);
}


// IntPoint methods
IntPoint::IntPoint(int x, int y, int z)
    : m_x(x), m_y(y), m_z(z)
{
}

std::ostream& operator<<(std::ostream &out, const IntPoint &p)
{
    out << "Point(" << p.m_x << ", " << p.m_y << ", " << p.m_z << ")";
    return out;
}


// Triangle methods
Triangle::Triangle(IntPoint p1, IntPoint p2, IntPoint p3)
    : m_points{p1, p2, p3}
{
}

std::ostream& Triangle::print(std::ostream& out) const
{
    out << "Triangle(" << m_points[0] << ", " << m_points[1] << ", " << m_points[2] << ")";
    return out;
}


// Circle methods
Circle::Circle(IntPoint p, int radius)
    : m_center{p}, m_radius{radius}
{
}

std::ostream& Circle::print(std::ostream& out) const
{
    out << "Circle(" << m_center << ", radius " << m_radius << ")";
    return out;
}

int& Circle::getRadius()
{
    return m_radius;
}

int getLargestRadius(std::vector<Shape*> &v)
{
    int maxRadius{};
    for (auto const shape : v)
        if (Circle *c = dynamic_cast<Circle*>(shape))
            if (maxRadius < c->getRadius())
                maxRadius = c->getRadius();
    return maxRadius;
}
