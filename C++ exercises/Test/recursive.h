#ifndef RECURSIVE_H_INCLUDED
#define RECURSIVE_H_INCLUDED

int factorial(int N);
int sumDigits(int number);
void decimalToBinary(int num);
#endif // RECURSIVE_H_INCLUDED
