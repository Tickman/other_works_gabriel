#ifndef BINARYSEARCH_H_INCLUDED
#define BINARYSEARCH_H_INCLUDED

int recursive_binarySearch(const int *array, int target, int min, int max);
int iterative_binarySearch(const int *array, int target, int min, int max);

#endif // BINARYSEARCH_H_INCLUDED
