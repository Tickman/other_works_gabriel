#ifndef ITEMS_H_INCLUDED
#define ITEMS_H_INCLUDED
#include <array>

namespace items
{
enum class Item_Type
{
    HEALTH_POTION,
    TORCH,
    ARROW,
    MAX_ITEMS,
};

int countTotalItems(std::array<int, static_cast<int>(Item_Type::MAX_ITEMS)> arr);
}
#endif // ITEMS_H_INCLUDED

