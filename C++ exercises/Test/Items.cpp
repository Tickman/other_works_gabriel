#include <array>
#include <numeric>

#include "Items.h"

namespace items
{
    int countTotalItems(std::array<int, static_cast<int>(Item_Type::MAX_ITEMS)> arr)
    {
        return std::accumulate(arr.begin(), arr.end(), 0);
    }
}
