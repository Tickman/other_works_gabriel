#ifndef MONSTERGAME_H_INCLUDED
#define MONSTERGAME_H_INCLUDED

#include <iostream>
#include <string>
#include <array>
#include <string_view>
#include <random>
#include <ctime>

inline std::mt19937 mersenne{ static_cast<std::mt19937::result_type>(std::time(nullptr)) };

class Creature
{
protected:
    std::string m_name{};
    char m_symbol{};
    int m_health{};
    int m_damage{};
    int m_gold{};

public:
    Creature(std::string name, char symbol, int health, int damage, int gold);

    std::string getName() const;
    char getSymbol() const;
    int getHealth() const;
    int getDamage() const;
    int getGold() const;

    void reduceHealth(int health);
    bool isDead() const;
    void addGold(int gold);
};

class Player : public Creature
{
    int m_level{1};

    Player(std::string& name);
public:
    Player();

    int getLevel() const;

    void levelUp();
    bool hasWon() const;

    friend std::istream& operator>> (std::istream& in, Player& p);
};


class Monster : public Creature
{

public:
    enum Type
    {
        DRAGON,
        ORC,
        SLIME,
        MAX_TYPES,
    };

private:
    struct MonsterData
    {
        std::string_view name{};
        char symbol{};
        int health{};
        int damage{};
        int gold{};
    };

    static constexpr std::array<MonsterData, Monster::MAX_TYPES> monsterData{
      { { "dragon", 'D', 20, 4, 100 },
        { "orc", 'o', 4, 2, 25 },
        { "slime", 's', 1, 1, 10 } }
    };

public:
    Monster(Type type);

    static Monster getRandomMonster();
};


#endif // MONSTERGAME_H_INCLUDED
