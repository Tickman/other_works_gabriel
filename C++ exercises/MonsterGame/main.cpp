#include "MonsterGame.h"

#include <iostream>
#include <string>
#include <random>
#include <ctime>


int getRandomNumber(int min, int max)
{
	std::uniform_int_distribution die{ min, max }; // we can create a distribution in any function that needs it
	return die(mersenne); // and then generate a random number from our global generator
}

//MonsterGame itself
void MonsterGame()
{

    std::cout << "Enter your name: ";
    Player player;

    std::cin >> player;
    std::cout << "Welcome, " << player.getName() << ".\n";
    //While of the entire game
    while (!player.hasWon() & !player.isDead())
    {
        Monster monster = Monster::getRandomMonster();
        std::cout << "You have encountered a " << monster.getName() << " (" << monster.getSymbol() << ").\n";
        char choice;
        //While of the battle
        while (!player.isDead())
        {
            //While of the choice to make
            do
            {
                std::cout << "(R)un or (F)ight:";
                std::cin >> choice;
                std::cin.ignore(32767, '\n');
            }
            while((choice != 'r') & (choice !='f'));
            if (choice == 'r')
            {
                if (getRandomNumber(0, 1) == 1)
                {
                    //Escapes successfully
                    std::cout << "You successfully fled.\n";
                    break;
                }
                else
                {
                    std::cout << "You failed to flee.\n";
                }
            }
            else
            {
                std::cout << "You hit the " << monster.getName() << " for " << player.getDamage() << " damage.\n";
                monster.reduceHealth(player.getDamage());
                if (monster.isDead())
                {
                    std::cout << "You killed the " << monster.getName() << ".\n";
                    player.levelUp();
                    std::cout << "You are now level " << player.getLevel() << ".\n";
                    std::cout << "You found " << monster.getGold() << " gold.\n";
                    player.addGold(monster.getGold());
                    break;
                }
            }
            std::cout << "The " << monster.getName() << " hit you for " << monster.getDamage() << " damage.\n";
            player.reduceHealth(monster.getDamage());
        }
    }
    if (player.hasWon())
    {
        std::cout << "You have won with an amount of " << player.getGold() << " gold.\nNow spend that money and buy some beer.\n";
    }
    else
    {
        std::cout << "You died at level " << player.getLevel() << " and with " << player.getGold() << " gold.\nToo bad you can't take it with you!\n";
    }
}

int main()
{
	MonsterGame();

	return 0;
}
