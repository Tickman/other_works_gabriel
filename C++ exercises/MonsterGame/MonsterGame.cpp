#include "MonsterGame.h"

#include <iostream>
#include <string>
#include <random>
#include <ctime>


// Monster methods
std::string Creature::getName() const
{
    return m_name;
}

char Creature::getSymbol() const
{
    return m_symbol;
}

int Creature::getHealth() const
{
    return m_health;
}

int Creature::getDamage() const
{
    return m_damage;
}

int Creature::getGold() const
{
    return m_gold;
}

Creature::Creature(std::string name, char symbol, int health, int damage, int gold)
    : m_name{name}, m_symbol{symbol}, m_health{health}, m_damage{damage}, m_gold{gold}
{
}

void Creature::reduceHealth(int health)
{
    m_health -= health;
}

bool Creature::isDead() const
{
    return m_health <= 0;
}

void Creature::addGold(int gold)
{
    m_gold += gold;
}

//Player methods

Player::Player(std::string& name)
    : Creature{name, '@', 10, 1, 0}
{
}

Player::Player()
    : Creature{"John", '@', 10, 1, 0}
{
}

int Player::getLevel() const
{
    return m_level;
}

void Player::levelUp()
{
    ++m_level;
    ++m_damage;
}

bool Player::hasWon() const
{
    return m_level >= 20;
}

std::istream& operator>> (std::istream& in, Player& p)
{
    std::string name;
    in >> name;
    p = Player(name);
    return in;
}


// Monster methods

Monster::Monster(Type type)
    : Creature{
        monsterData[type].name.data(),
        monsterData[type].symbol,
        monsterData[type].health,
        monsterData[type].damage,
        monsterData[type].gold}
{
}

Monster Monster::getRandomMonster()
{
    std::uniform_int_distribution randomMonster{ 0, Type::MAX_TYPES-1 };

    return Monster(static_cast<Type>(randomMonster(mersenne)));
}

