class Professor:
    __name = "John"
    __department = "meh"
    __college = "UB"

    def getName(self):
        return self.__name

    def setName(self, name):
        self.__name = name

    def getDepartment(self):
        return self.__department

    def setDepartment(self, department):
        self.__department = department

    def getCollege(self):
        return self.__college

    def setCollege(self, college):
        self.__college = college


professor = Professor()
print("Please, provide the following details:\n")
professor.setName(input("Enter your name: "))
professor.setDepartment(input("Enter your department: "))
professor.setCollege(input("Enter your college: "))
print('''
-----------------------------
Name:       {}
Department: {}
College:    {} 
        '''.format(professor.getName(), professor.getDepartment(), professor.getCollege()))
