def six_by_seven(number):
    if (number % 6 == 0) and (number % 7 == 0):
        return 'Universe'
    elif number % 6 == 0:
        return 'Food'
    elif number % 7 == 0:
        return 'Good'
    else:
        return 'Oops'


def dec_bin():
    for i in range(1, 1001):
        binstring1 = str(bin(i)[2:])
        binstring2 = ""
        string1 = str(i)
        string2 = ""
        for j in reversed(binstring1):
            binstring2 += j
        for k in reversed(string1):
            string2 += k
        if string1 == string2 and binstring1 == binstring2:
            print(int(string1), bin(int(string1))[2:])


dec_bin()