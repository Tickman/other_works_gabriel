def len_int(number):
    if type(number) is int:
        return len(str(abs(number)))
    else:
        raise TypeError("Provide only integer input")


def str_cmp(str1, str2):
    if str1.lower() == str2.lower():
        return True
    else:
        return False


def str_anagram(str1, str2):
    list1 = list(str1)
    list2 = list(str2)
    if (len(list1) == len(list2)) and (set(list1) - set(list2) == set()):
        return True
    else:
        return False


def num(number):
    if type(number) is int:
        return int(number)
    elif type(number) is float:
        return float(number)
    elif type(number) is str:
        try:
            if "." in number:
                number = float(number)
                return number
            else:
                number = int(number)
                return number
        except Exception as e:
            raise ValueError("Could not convert string to int or float")
    else:
        raise TypeError("Not a valid input")



print(num('34.5'))
