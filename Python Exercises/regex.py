import re


def Is_Floating_Number():
    N = input()
    if re.search(r"^[+\-]?\d*\.\d+", N):
        return True
    else:
        return False


def regex_split():
    s = input()
    if re.search(r"^\d+?[\d,|.\d]+\d$", s):
        p = re.compile(r"[.,]")
        print(p.sub("\n", s))
    else:
        raise TypeError


def start_or_end():
    S = input()
    k = input()
    p = re.compile(k)
    r = p.search(S)
    if not r:
        print("(-1, -1)")
    while r:
        print("({}, {})".format(r.start(), r.end() - 1))
        r = p.search(S, r.start() + 1)


def regex_sub(S):
    # p = re.compile(r'(?s)^(?!&).*\b&&\b.*(?<!&)$')
    p = re.compile(r" &&(?= )")
    p2 = re.compile(r" \|\|(?= )")
    it = p.finditer(S)
    it2 = p2.finditer(S)

    for e1, e2 in zip(it, it2):
        S = p.sub(r" and ", S)
        S = p2.sub(r" or ", S)
    print(S)


def validate_phone_number():
    number = input()
    if re.match(r"^[789][\d]{8}$", number):
        return True
    else:
        return False


def validate_email(email):
    p = re.compile(r"^([a-zA-Z])[\w\-.]*@[a-zA-Z]+\.[a-zA-Z]{1,3}$")
    if p.match(email):
        return True
    else:
        return False


def hex_color(S):
    p = re.compile(r"[^\n]#[0-9a-fA-F]{3,6}")
    it = p.findall(S)
    for elem in it:
        print(elem[1:])


"""
#BED
{
    color: #FfFdF8; background-color:#aef;
    font-size: 123px;
    background: -webkit-linear-gradient(top, #f9f9f9, #fff);
}
#Cab
{
    background-color: #ABC;
    border: 2px dashed #fff;
}   
"""


def validate_UID(uid):
    if (
            not re.search(r"[A-Z]{2,}", uid)
            or not re.search(r"(?:.*[0-9].*){3}", uid)
            or not re.search(r"^(?!.*(.).*\1)[0-9a-zA-Z]{10}$", uid)
    ):
        print("Invalid")
    else:
        print("Valid")


def validate_credit_card(num):
    p = re.compile(r"^[456][0-9]{3}-[0-9]{4}-[0-9]{4}-[0-9]{4}$")  # With hyphen
    p2 = re.compile(r'^(?!.*(.)\1\1\1.*)[456][0-9]{15}$')  # With no hyphen
    if p.match(num):
        num = re.sub(r'-', r'', num)
    if p2.match(num):
        print('Valid')
    else:
        print('Invalid')


def validate_postal_card(card):
    p = re.compile(r'^(?!.*(.).\1.*)[1-9]\d{5}$')
    return bool(p.match(card))


def matrix(matrix_string):
    matrix_text = []
    normal_string = []
    length = (matrix_string.find('\n', 1))
    matrix_text = matrix_string.split('\n')
    del matrix_text[0], matrix_text[-1]
    for i in range(len(matrix_text)):
        matrix_text[i] = list(matrix_text[i])
    for i in range(len(matrix_text[0])):
        for j in range(len(matrix_text)):
            normal_string.append(matrix_text[j][i])
    normal_string = ''.join(normal_string)
    p = re.compile(r'(?<=[0-9a-zA-Z]\b)[!@#$%& ]+(?=[0-9a-zA-Z])')
    normal_string = p.sub(r' ', normal_string)
    print(normal_string)


"""
Tsi
h%x
i #
sM 
$a 
#t%
ir!
"""


def validate_date(date):
    # YYYY-MM-DD
    date_list = []
    p = re.compile(r'[0-9]{4}-[0-9]{2}-[0-9]{2}')
    if p.match(date):
        date_list = date.split('-')
        # Does not take in account months with only 30 days or February, but it would be easy to deal with that problem.
        if (int(date_list[1]) <= 12) and (int(date_list[2]) <= 31):
            return True
        else:
            return False
    else:
        return False


def tetravocalic():
    my_list = []
    p = re.compile(r'.*[aeiou]{4}.*', re.DOTALL)
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def hexaconsonantal():
    my_list = []
    p = re.compile(r'.*[^aeiou\n]{6}.*')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def repeat_letter():
    my_list = []
    p = re.compile(r'(?=.*(.).*\1.*\1.*\1.*\1.*)')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def acronym(string):
    p = re.compile(r'\b\w|[a-z][A-Z]')
    my_list = p.findall(string)
    for i in range(len(my_list)):
        if my_list[i][0].islower():
            my_list[i] = my_list[i][1:]
    print(''.join(my_list).upper())


def palindrome():
    my_list = []
    p = re.compile(r'([a-z])([a-z])([a-z])\2\1$')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.match(line):
                my_list.append(line)
    print(my_list)

def double_double():
    my_list = []
    p = re.compile(r'(?=.*(.)\1.\1\1)')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def repetitive_words():
    my_list = []
    p = re.compile(r'^(.*)\1$')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def all_vowels():
    my_list = []
    p = re.compile(r'(?=.*[a].*)(?=.*[e].*)(?=.*[i].*)(?=.*[o].*)(?=.*[u].*)[a-z]{1,9}')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def unique_letters():
    my_list = []
    p = re.compile(r'(?!.*(.).*\1)[a-z]{10,}')
    with open('dictionary.txt', 'r') as d:
        for line in d:
            if p.search(line):
                my_list.append(line)
    print(my_list)


def CamelCase_to_underscore(string):
    def underscore(match):
        return match.group(1)+"_"+match.group(2).lower()

    p = re.compile(r'([a-z])([A-Z])')
    print(p.sub(underscore, string))

