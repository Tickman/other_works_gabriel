import math
import operator as o
import random as r
from datetime import datetime
import collections as c
import itertools as it
import re


def Luhn_Formula(number):
    lista = [int(n) for n in str(number)]
    suma = 0
    for i in range(1, len(lista), 2):
        lista[i] *= 2
        if lista[i] > 9:
            lista[i] -= 9
    for i in lista:
        suma += i
    if suma % 10 == 0:
        return True
    else:
        return False


def wut(string):
    lista = string.split(",")
    for i in range(0, len(lista)):
        lista[i] = round(math.sqrt(2 * 50 * int(lista[i]) / 30))
    return lista


def matrix(X, Y):
    matrix2d = []
    for i in range(X):
        row = []
        for j in range(Y):
            row.append(i * j)
        matrix2d.append(row)
    return matrix2d


class Student:
    def __init__(self, name, age, grade):
        self.name = name
        self.age = age
        self.grade = grade


def sorted_students(student_list):
    student_list = sorted(student_list, key=o.attrgetter("name", "age", "grade"))
    for i in range(len(student_list)):
        student_list[i] = (
            student_list[i].name,
            student_list[i].age,
            student_list[i].grade,
        )
    return student_list


"""
student_list = [
    Student("Tom", 19, 80),
    Student("John", 20, 90),
    Student("Jony", 17, 91),
    Student("Jony", 17, 93),
    Student("Json", 21, 85),
]
"""


def Create_generator(n):
    my_list = range(n)
    for i in my_list:
        if i % 7 == 0:
            yield i


"""
mygen = Create_generator(50)

for i in mygen:
    print(i)
"""


def robot_moving(orders):
    x, y = 0, 0
    with open(orders) as orde:
        for line in orde:
            order, steps = line.split()
            if order == "UP":
                y += int(steps)
            elif order == "DOWN":
                y -= int(steps)
            elif order == "LEFT":
                x += int(steps)
            elif order == "RIGHT":
                x -= int(steps)
            else:
                raise ImportError("Bad order")
    return round(math.sqrt(x ** 2 + y ** 2))


def word_frequency(string):
    lista = string.split()
    word_dict = {}
    for elem in lista:
        if elem in word_dict:
            word_dict[elem] += 1
        else:
            word_dict[elem] = 1
    return word_dict


"""
string = "New to Python or choosing between Python 2 and Python 3? Read Python 2 or Python 3."
dic = word_frequency(string)
for i in dic:
    print("{} : {}".format(i, dic[i]))
"""


def password_generator(strength):
    # strength:  0->weak, only lowercase  1->medium, low and uppercase  2->strong, numbers 3->very strong, symbols
    # weak: 4-8 char, medium: 8-12 char, strong: 12-16 char, very strong: 16-20 char
    password = ""
    length = r.randint(4 + strength * 4, 8 + strength * 4)
    for i in range(length):
        selector = r.randint(0, strength)
        if selector == 0:
            password += chr(r.randint(97, 122))
        elif selector == 1:
            password += chr(r.randint(65, 90))
        elif selector == 2:
            password += chr(r.randint(48, 57))
        elif selector == 3:
            password += chr(r.randint(35, 38))
        else:
            continue
    return password


def The_Minion_Game(string):
    stuart_score = 0
    kevin_score = 0
    vowels = "AEIOU"
    forbidden_list = []
    for i in range(0, len(string)):
        if string[i] not in vowels:
            # Stuart
            for j in range(i, len(string)):
                word = string[i : j + 1]
                if word not in forbidden_list:
                    forbidden_list.append(word)
                    for k in range(i, len(string) - len(word) + 1):
                        if string.find(word, k, k + len(word)) != -1:
                            stuart_score += 1

        else:
            # Kevin
            for j in range(i, len(string)):
                word = string[i : j + 1]
                if word not in forbidden_list:
                    forbidden_list.append(word)
                    for k in range(i, len(string) - len(word) + 1):
                        if string.find(word, k, k + len(word)) != -1:
                            kevin_score += 1

    if stuart_score == kevin_score:
        print("Draw")
    elif stuart_score > kevin_score:
        print("Stuart {}".format(stuart_score))
    else:
        print("Kevin {}".format(kevin_score))


def Merge_The_Tools():
    string = input()
    while True:
        try:
            divider = int(input())
            if len(string) % divider != 0:
                print("Introduce a factor of the length of the string")
                continue
        except:
            print("Introduce an integer")
            continue
        break
    for i in range(int(len(string) / divider)):
        substr = string[(i * divider) : (i * divider + divider)]
        reversed_substr = "".join(list(reversed(substr)))
        for j in range(len(substr)):
            if (
                reversed_substr.find(reversed_substr[j], j + 1, len(reversed_substr))
                != -1
            ):
                reversed_substr = (
                    reversed_substr[: j + 1].replace(reversed_substr[j], " ")
                    + reversed_substr[j + 1 :]
                )
                substr = "".join((list(reversed(reversed_substr))))
        substr = substr.replace(" ", "")
        print(substr)


def Time_Delta():
    # Day dd Mon yyyy hh:mm:ss +xxxx
    cases = int(input())
    test_cases = []
    for i in range(cases):
        dt1 = datetime.strptime(input(), "%a %d %b %Y %H:%M:%S %z")
        dt2 = datetime.strptime(input(), "%a %d %b %Y %H:%M:%S %z")
        diff = int((dt1 - dt2).total_seconds())
        test_cases.append(diff)
    for elem in test_cases:
        print(elem)


# 2
# Sun 10 May 2015 13:54:36 -0700
# Sun 10 May 2015 13:54:36 -0000
# Sat 02 May 2015 19:54:36 +0530
# Fri 01 May 2015 13:54:36 -0000


def find_Angle_MBC():
    # Right triangle (ABC) at point B. M is located at the intersection of point B with the middle point in AC
    while True:
        try:
            AB = int(input())
            if AB <= 0:
                print("Only natural numbers")
                continue
        except:
            print("Only natural numbers")
            continue
        break
    while True:
        try:
            BC = int(input())
            if BC <= 0:
                print("Only natural numbers")
                continue
        except:
            print("Only natural numbers")
            continue
        break
    AC = math.sqrt(AB ** 2 + BC ** 2)
    MC = AC / 2
    theta = math.asin(MC / BC)
    print("{}\u00b0".format(round(math.degrees(theta))))


def No_Idea():
    while True:
        n, m = input().split()
        try:
            n = int(n)
            m = int(m)
            if ((n >= 1e5) or (n < 1)) or ((m >= 1e5) or (m < 1)):
                print("Number between 1 and 10^5")
                continue
        except:
            print("Only natural numbers")
            continue
        break

    while True:
        my_array = input().split()
        if len(my_array) != n:
            print("Number of variables does not match with the previous input")
            continue
        try:
            my_array = [int(x) for x in my_array]
            for elem in my_array:
                if 1 <= elem <= 1e9:
                    continue
                else:
                    print("Number between 1 and 10^9")
                    break
            else:
                break
        except:
            print("Only natural numbers")
            continue

    while True:
        like_array = input().split()
        if len(like_array) != m:
            print("Number of variables does not match with the previous input")
            continue
        try:
            like_array = [int(x) for x in like_array]
            for elem in like_array:
                if 1 <= elem <= 1e9:
                    continue
                else:
                    print("Number between 1 and 10^9")
                    break
            else:
                break
        except:
            print("Only natural numbers")
            continue

    while True:
        dislike_array = input().split()
        if len(dislike_array) != m:
            print("Number of variables does not match with the previous input")
            continue
        try:
            dislike_array = [int(x) for x in dislike_array]
            for elem in dislike_array:
                if 1 <= elem <= 1e9:
                    continue
                else:
                    print("Number between 1 and 10^9")
                    break
            else:
                break
        except:
            print("Only natural numbers")
            continue

    happiness = 0
    for elem in my_array:
        if elem in like_array:
            happiness += 1
        elif elem in dislike_array:
            happiness -= 1
        else:
            continue
    print(happiness)


def Word_Order():
    while True:
        try:
            n = int(input())
            if 1 <= n <= 1e5:
                break
            else:
                print("Only natural numbers")
                continue
        except:
            print("Only natural numbers")
            continue
    words_list = []
    for i in range(n):
        words_list.append(input())
    words_set = c.OrderedDict()
    for elem in words_list:
        words_set[elem] = None
    print(len(words_set))
    for elem in words_set:
        print(words_list.count(elem), end=" ")


# 4
# bcdef
# bcdefg
# bcde
# bcdef


def Compress_the_String():
    while True:
        string = input()
        try:
            int(string)
            break
        except:
            print("Introduce a valid number")
    forbidden_number = 0
    i = 0
    while i < len(string):
        ocurrences = [0, int(string[i])]
        for j in range(i, len(string)):
            if string[i] == string[j]:
                try:
                    if string[j + 1] != string[j]:
                        ocurrences[0] += 1
                        break
                except:
                    pass
                ocurrences[0] += 1
                forbidden_number = string[i]
        print("({}, {})".format(ocurrences[0], ocurrences[1]), end=" ")
        i = i + ocurrences[0]


# 1222311


def Company_Logo():
    while True:
        string = input()
        if 3 < len(string) < 1e4:
            break
        else:
            print("Introduce a longer string")
    occurrence = c.OrderedDict()
    for ch in string:
        if ch in occurrence:
            occurrence[ch] += 1
        else:
            occurrence[ch] = 1
    max_value = max(i for i in occurrence.values())
    counter = 0
    for i in range(max_value, 0, -1):
        for key, value in occurrence.items():
            if value == i:
                print(key, value)
                counter += 1
        if counter >= 3:
            break


# aabbbccde


def Palindromic_Triangle(number):
    if 0 < number < 10:
        for i in range(1, number + 1):
            # No strings
            # print(*(i for i in range(1, i + 1)), *(i for i in range(i - 1, 0, -1)))
            # Just a bit of strings
            # print("".join([str(i) for i in range(1, i + 1)]) + "".join([str(i) for i in range(i - 1, 0, -1)]))
            # Cooler one found on the internet
            print(pow((10 ** i - 1) // 9, 2))
    else:
        print("Introduce a number between 0 and 10")


def Iterables():
    while True:
        N = input()
        try:
            N = int(N)
            if 1 <= N <= 10:
                break
            else:
                print("Number between 1 and 10")
                continue
        except:
            print("Number between 1 and 10")
    while True:
        my_list = input().split()
        if len(my_list) == N:
            break
        else:
            print("Number of variables in list does not match with previous input")
    while True:
        k = input()
        try:
            k = int(k)
            if 1 <= k <= N:
                break
            else:
                print("Number between 1 and N")
                continue
        except:
            print("Number between 1 and N")

    tuple_list = list((it.combinations("".join(my_list), k)))
    counter = 0
    for i in range(len(tuple_list)):
        if "a" in tuple_list[i]:
            counter += 1
    result = counter / len(tuple_list)
    print("{0:.4g}".format(result))


def Maximizer():
    K, M = map(int, input().split())
    # list_of_lists = [map(int, input.split())[1:] for _ in range(K)]
    list_of_lists = []
    for i in range(K):
        my_list = input().split()
        while True:
            try:
                my_list = [int(i) for i in my_list]
                length = my_list[0]
                my_list = my_list[1:]
                if length == len(my_list) and (1 <= length <= 7):
                    break
                else:
                    print(
                        "Given Length does not match with list length or Length is too large"
                    )
            except:
                print("Enter a valid format")
        list_of_lists.append(my_list)
    # Looked on the internet for this
    result = map((lambda y: sum(j ** 2 for j in y) % M), it.product(*list_of_lists))
    print(max(result))


"""
3 1000
2 5 4
3 7 8 9 
5 5 7 8 9 10
"""


def Triangle_Quest():
    for i in range(1, int(input())):
        print((10 ** i - 1) * i // 9)


def Complex_Numbers():
    C = complex(*map(int, input().split()))
    D = complex(*map(int, input().split()))
    print("{:.2f}{:+.2f}i".format((C + D).real, (C + D).imag))
    print("{:.2f}{:+.2f}i".format((C - D).real, (C - D).imag))
    print("{:.2f}{:+.2f}i".format((C * D).real, (C * D).imag))
    print("{:.2f}{:+.2f}i".format((C / D).real, (C / D).imag))
    print("{:.2f}{:+.2f}i".format(abs(C).real, abs(C).imag))
    print("{:.2f}{:+.2f}i".format(abs(D).real, abs(D).imag))


def Athlete_Sort():
    N, M = map(int, input().split())
    # total_list = [list(map(int, input().split())) for _ in range(N)]
    total_list = []
    for i in range(N):
        while True:
            my_list = list(map(int, input().split()))
            if len(my_list) == M:
                total_list.append(my_list)
                break
            else:
                continue
    K = int(input())
    total_list = sorted(total_list, key=lambda x: x[K])
    for elem in total_list:
        print(*(j for j in elem), end=" ")
        print()


def Sorting():
    # Lowercase > Uppercase > Digits
    low = []
    up = []
    odd = []
    even = []
    S = input()
    for char in S:
        if char.islower():
            low.append(char)
        elif char.isupper():
            up.append(char)
        elif char.isnumeric():
            if int(char) % 2 == 0:
                even.append(char)
            else:
                odd.append(char)
        else:
            raise TypeError
    print("".join(sorted(low))+"".join(sorted(up))+"".join(odd)+"".join(even))


def Word_Score():
    n = int(input())
    while True:
        my_list = input().split()
        if len(my_list) == n:
            break
        else:
            continue
    score = 0
    for string in my_list:
        counter = 0
        for char in string:
            if char in 'aeiouy':
                counter += 1
        score = score + 2 if counter % 2 == 0 else score + 1
    print(score)
