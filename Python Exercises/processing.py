def is_one_char_diff(word1, word2):
    word1 = word1.lower()
    word2 = word2.lower()
    checker = 0
    if len(word1) == len(word2):
        for i in range(0, len(word1)):
            if word1[i] == word2[i]:
                checker += 1
        if checker >= len(word1) - 1:
            return True
        else:
            return False
    else:
        return False


def is_alpha_order(word):
    word = word.lower()
    counter = 0
    for i in word:
        try:
            if ord(i) <= ord(word[word.index(i) + 1]):
                counter += 1
            else:
                continue
        except:
            pass

    if counter == len(word) - 1:
        return True

    for i in word:
        try:
            if ord(i) >= ord(word[word.index(i) + 1]):
                continue
            else:
                return False
        except:
            pass
    return True


def is_alpha_order_sentence(sentence):
    lista = sentence.split()
    for word in lista:
        word = word.lower()
        counter = 0
        for i in word:
            try:
                if ord(i) <= ord(word[word.index(i) + 1]):
                    counter += 1
                else:
                    continue
            except:
                pass

        if counter == len(word) - 1:
            return True

        for i in word:
            try:
                if ord(i) >= ord(word[word.index(i) + 1]):
                    continue
                else:
                    return False
            except:
                pass
        return True

#Casi hecho
def max_nested_braces(string):
    reversed_string = "".join(list(reversed(string)))
    left_braces = 0
    right_braces = 0
    min_length = 0
    if ("{" not in string) and ("}" not in string):
        return 0
    for i in range(0, len(string)):
        if "{" == string[i]:
            left_braces += 1
            if string.find("}", min_length, len(string)) == -1:
                return -1
            else:
                if string[i+1] == "}":
                    left_braces -= 1
                else:
                    min_length = string.find("}", min_length, len(string))
    min_length = 0
    for j in range(0, len(reversed_string)):
        if "}" == reversed_string[j]:
            right_braces += 1
            if reversed_string.find("{", min_length, len(reversed_string)) == -1:
                return -1
            else:
                if reversed_string[j + 1] == "{":
                    right_braces -= 1
                else:
                    min_length = reversed_string.find("{", min_length, len(reversed_string))

    if left_braces == right_braces:
        return left_braces
    else:
        return -1


