def product(list):
    total = 1
    for i in list:
        total *= i
    return total


def nth_lowest(lista, index=1):
    return sorted(set(lista))[index - 1]


def word_slices(string):
    lista = []
    if len(string) < 3:
        return [string]
    else:
        for i in range(0, len(string)):
            for j in range(i, len(string)+1):
                if (i == j) or (len(string[i:j]) < 2):
                    continue
                else:
                    lista.append(string[i:j])
        return lista
            


print(word_slices("table"))
